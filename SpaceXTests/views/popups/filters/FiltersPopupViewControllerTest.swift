//
//  FiltersPopupViewControllerTest.swift
//  FiltersPopupViewControllerTest
//
//  Created by Joao Fonseca on 17/08/2021.
//

@testable import SpaceX
import SnapshotTesting
import SwiftyMocky
import Swinject
import XCTest

class FiltersPopupViewControllerTest: XCTestCase {
    override func setUp() {
        super.setUp()
    }

    override func tearDown() {
        super.tearDown()
    }

    func testSnapShotViewControllerWithData() {
        let filter = Filter(
            order: NSLocalizedString("FILTER_DESC", comment: ""),
            year: NSLocalizedString("FILTER_ALL_YEARS", comment: ""),
            onlySuccess: false)
        let container = setUpContainer()
        let filterToRestore = FilterHelper.share.get()
        FilterHelper.share.set(filter: filter)
        if let sut = container.resolve(FiltersPopupViewController.self) {
            assertSnapshot(matching: sut, as: .image)
        }

        FilterHelper.share.set(filter: filterToRestore)
    }

    func testShouldUpdateFiltersTest() {
        let container = setUpContainer()
        let filterToRestore = FilterHelper.share.get()
        FilterHelper.share.set(filter: Filter(order: "DESC", year: "2002", onlySuccess: false))
        if let sut = container.resolve(FiltersPopupViewController.self) {
            sut.updateFilters()

            let filter = FilterHelper.share.get()

            XCTAssertEqual(filter.order, "DESC")
            XCTAssertEqual(filter.year, "2002")
            XCTAssertEqual(filter.onlySuccess, false)
        }

        FilterHelper.share.set(filter: filterToRestore)
    }

    // MARK: - Set Up Container

    private func setUpContainer() -> Container {
        let container = DependencyInjection.sharedContainer()

        container.register(FiltersPopupViewController.self) { _ in FiltersPopupViewController() }

        return container
    }
}
