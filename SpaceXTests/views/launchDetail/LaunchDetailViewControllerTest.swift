//
//  LaunchDetailViewControllerTest.swift
//  LaunchDetailViewControllerTest
//
//  Created by Joao Fonseca on 17/08/2021.
//

@testable import SpaceX
import SnapshotTesting
import SwiftyMocky
import Swinject
import XCTest

class LaunchDetailViewControllerTest: XCTestCase {
    override func setUp() {
        super.setUp()
    }

    override func tearDown() {
        super.tearDown()
    }

    func testSnapShotViewControllerWithData() {
        let launches = MockInfoBuilder.shared.getLauchInformtion()
        let container = setUpContainer()
        if let sut = container.resolve(LaunchDetailViewController.self), let launch = launches.last as? Launch {
            sut.setLaunch(launch: launch)
            assertSnapshot(matching: sut, as: .image)
        }
    }

    func testShouldInitialize() {
        let viewModelMock = LaunchDetailViewModelMock()
        let launches = MockInfoBuilder.shared.getLauchInformtion()

        let container = setUpContainer()
        if let sut = container.resolve(LaunchDetailViewController.self), let launch = launches.first {
            sut.viewModel = viewModelMock

            let expectation = expectation(description: "should initialize launch")

            Perform(viewModelMock, .initialize(.any, perform: { launchUpdate in
                XCTAssertEqual(launchUpdate.mission_name, launch?.mission_name ?? "")
                expectation.fulfill()
            }))

            sut.setLaunch(launch: launch!)

            waitForExpectations(timeout: 3)
        }
    }

    func testShouldOpenLink() {
        let viewCoordinatorMock = LaunchDetailViewCoordinatorMock()
        let container = setUpContainer()
        if let sut = container.resolve(LaunchDetailViewController.self), let url = URL(string: "www.google.com") {
            sut.viewCoordinator = viewCoordinatorMock

            let expectation = expectation(description: "should open external link")

            Perform(viewCoordinatorMock, .openExternalLink(url: .any, perform: { url in
                XCTAssertEqual(url.path, "www.google.com")
                expectation.fulfill()
            }))

            sut.openLink(url: url)

            waitForExpectations(timeout: 3)
        }
    }

    // MARK: - Set Up Container

    private func setUpContainer() -> Container {
        let container = DependencyInjection.sharedContainer()

        container.register(LaunchDetailViewController.self) { _ in LaunchDetailViewController() }

        return container
    }
}
