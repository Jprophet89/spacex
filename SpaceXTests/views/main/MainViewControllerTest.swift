//
//  MainViewControllerTest.swift
//  MainViewControllerTest
//
//  Created by Joao Fonseca on 16/08/2021.
//

@testable import SpaceX
import SnapshotTesting
import SwiftyMocky
import Swinject
import XCTest

class MainViewControllerTest: XCTestCase {
    override func setUp() {
        super.setUp()
    }

    override func tearDown() {
        super.tearDown()
    }

    func testSnapShotViewControllerWithData() {
        let launches = MockInfoBuilder.shared.getLauchInformtion()
        let companyInfo = MockInfoBuilder.shared.getCompanyInformtion()
        // MockRepositories
        let launchRepositoryMock = LaunchRepositoryMock()
        Given(launchRepositoryMock, .get(willReturn: launches))
        let companyRepositoryMock = CompanyRepositoryMock()
        Given(companyRepositoryMock, .getInfo(willReturn: companyInfo))

        let container = setUpContainer(companyRepositoryMock: companyRepositoryMock, launchRepositoryMock: launchRepositoryMock)
        if let sut = container.resolve(MainViewController.self) {
            let expect = expectation(description: "wait for the view to load")
            expect.isInverted = true
            waitForExpectations(timeout: 1)
            assertSnapshot(matching: sut, as: .image)
        }
    }

    func testSnapShotViewControllerWithNoData() {
        // MockRepositories
        let launchRepositoryMock = LaunchRepositoryMock()
        Given(launchRepositoryMock, .get(willReturn: []))
        let companyRepositoryMock = CompanyRepositoryMock()
        Given(companyRepositoryMock, .getInfo(willReturn: nil))

        let container = setUpContainer(companyRepositoryMock: companyRepositoryMock, launchRepositoryMock: launchRepositoryMock)
        if let sut = container.resolve(MainViewController.self) {
            let expect = expectation(description: "wait for the view to load")
            expect.isInverted = true
            waitForExpectations(timeout: 1)
            assertSnapshot(matching: sut, as: .image)
        }
    }

    func testOpenFilters() {
        // Mock repository
        let launches = MockInfoBuilder.shared.getLauchInformtion()
        let launchRepository = LaunchRepositoryMock()
        Given(launchRepository, .get(willReturn: launches))
        Perform(launchRepository, .refresh(filter: .any, completion: .any, perform: { _, completion in
            completion(.success(launches))
        }))
        // replace mock repository on service
        let launchService = DefaultLaunchService()
        launchService.repository = launchRepository
        // replace service on ViewModel
        let viewModel = DefaultMainViewModel()
        viewModel.launchesService = launchService

        let container = setUpContainer(viewModel)
        if let sut = container.resolve(MainViewController.self) {
            let coordinatorMock = MainViewCoordinatorMock()
            sut.viewCoordinator = coordinatorMock

            let expectation = expectation(description: "should open filters")

            Perform(coordinatorMock, .presentFilters(from: .any, perform: { _ in
                XCTAssert(true)
                expectation.fulfill()
            }))

            sut.openFilters()

            waitForExpectations(timeout: 3)
        }
    }

    func testShouldUpdateFilter() {
        let viewModel = MainViewModelMock()
        let container = setUpContainer(viewModel)
        if let sut = container.resolve(MainViewController.self) {
            let expectation = expectation(description: "should update filters")
            Perform(viewModel, .updatedFilters(perform: {
                XCTAssert(true)
                expectation.fulfill()
            }))

            sut.updateFilter()

            waitForExpectations(timeout: 3)
        }
    }

    func testShouldSelectedLaunche() {
        let launches = MockInfoBuilder.shared.getLauchInformtion()
        let container = setUpContainer(MainViewModelMock())
        if let sut = container.resolve(MainViewController.self), let launch = launches.first {
            let coordinatorMock = MainViewCoordinatorMock()
            sut.viewCoordinator = coordinatorMock

            let expectation = expectation(description: "should present launch")

            Perform(coordinatorMock, .presentLaunch(from: .any, launch: .any, perform: { _, _ in
                XCTAssert(true)
                expectation.fulfill()
            }))

            sut.selectedLaunch(launch: launch!)

            waitForExpectations(timeout: 3)
        }
    }

    func testShouldRefreshInformation() {
        let viewModelMock = MainViewModelMock()
        let container = setUpContainer(viewModelMock)
        if let sut = container.resolve(MainViewController.self) {
            let expectation = expectation(description: "should select launch")
            Perform(viewModelMock, .refresh(perform: {
                XCTAssert(true)
                expectation.fulfill()
            }))

            sut.refreshInformation()

            waitForExpectations(timeout: 3)
        }
    }

    func testShouldHaveAlert() {
        let viewModelMock = DefaultMainViewModel()
        let container = setUpContainer(viewModelMock)
        if let sut = container.resolve(MainViewController.self) {
            sut.showError(error: .unknown)
            XCTAssert(sut.hasAlert)
        }
    }

    // MARK: - Set Up Container

    private func setUpContainer(_ viewModel: MainViewModel? = nil, companyRepositoryMock: CompanyRepository? = nil, launchRepositoryMock: LaunchRepository? = nil) -> Container {
        let container = DependencyInjection.sharedContainer()

        container.register(CompanyService.self) { _ in
            let companyService = DefaultCompanyService()
            if let companyRepositoryMock = companyRepositoryMock {
                companyService.repository = companyRepositoryMock
            }

            return companyService
        }

        container.register(LaunchService.self) { _ in
            let launchService = DefaultLaunchService()
            if let launchRepositoryMock = launchRepositoryMock {
                launchService.repository = launchRepositoryMock
            }

            return launchService
        }
        container.register(MainViewModel.self) { _ in
            if let viewModel = viewModel {
                return viewModel
            } else {
                return DefaultMainViewModel()
            }
        }
        container.register(MainViewController.self) { _ in MainViewController() }

        return container
    }
}
