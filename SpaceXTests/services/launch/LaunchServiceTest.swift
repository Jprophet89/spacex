//
//  LaunchServiceTest.swift
//  LaunchServiceTest
//
//  Created by Joao Fonseca on 16/08/2021.
//

@testable import SpaceX
import SwiftyMocky
import Swinject
import XCTest

class LaunchServiceTest: XCTestCase {
    override func setUp() {
        super.setUp()
    }

    override func tearDown() {
        super.tearDown()
    }

    // MARK: - Set Up Container

    private func setUpContainer(_ launchRepository: LaunchRepositoryMock) -> Container {
        let container = DependencyInjection.sharedContainer()

        container.register(LaunchRepository.self) { _ in launchRepository }
        container.autoregister(LaunchService.self, initializer: DefaultLaunchService.init)

        return container
    }

    func testGet() {
        let launchRepository = LaunchRepositoryMock()
        let container = setUpContainer(launchRepository)
        if let sut = container.resolve(LaunchService.self) {
            // Simulate error
            Given(launchRepository, .get(willReturn: nil))

            var arrayOfLaunch = sut.get()
            XCTAssertNil(arrayOfLaunch)

            // test empty array
            Given(launchRepository, .get(willReturn: []))

            arrayOfLaunch = sut.get()
            XCTAssertNotNil(arrayOfLaunch)
            XCTAssert(arrayOfLaunch?.isEmpty ?? false)

            // test with data
            let launches = MockInfoBuilder.shared.getLauchInformtion()
            Given(launchRepository, .get(willReturn: launches))

            arrayOfLaunch = sut.get()
            XCTAssertNotNil(arrayOfLaunch)
            if let arrayOfLaunch = arrayOfLaunch {
                XCTAssert(!arrayOfLaunch.isEmpty)
                XCTAssertEqual(arrayOfLaunch.count, 3)
            }

        } else {
            XCTAssert(false)
        }
    }

    func testGetDate() {
        let launchRepository = LaunchRepositoryMock()
        let container = setUpContainer(launchRepository)
        if let sut = container.resolve(LaunchService.self) {
            let launches = MockInfoBuilder.shared.getLauchInformtion()

            guard let firstLaunch = launches.first else {
                XCTAssert(false)
                return
            }
            // Check date should be nil
            var date = sut.getDate(launch: nil)
            XCTAssertNil(date)
            // Check date should be the same as mock
            let dateToTestFirst = DateComponents(calendar: Calendar.current, timeZone: .current, era: nil, year: 2020, month: 12, day: 13, hour: 17, minute: 30, second: 00, nanosecond: 0, weekday: nil, weekdayOrdinal: nil, quarter: nil, weekOfMonth: nil, weekOfYear: nil, yearForWeekOfYear: nil).date
            date = sut.getDate(launch: firstLaunch)
            XCTAssertEqual(date, dateToTestFirst)
        }
    }

    func testGetRocketInfo() {
        let notAvailable = NSLocalizedString("NOT_AVAILABLE", comment: "")
        let launchRepository = LaunchRepositoryMock()
        let container = setUpContainer(launchRepository)
        if let sut = container.resolve(LaunchService.self) {
            let launches = MockInfoBuilder.shared.getLauchInformtion()

            guard let firstLaunch = launches.first else {
                XCTAssert(false)
                return
            }
            // Check date should be nil
            var information = sut.getRocketInfo(launch: nil)
            XCTAssertEqual(information, "\(notAvailable) / \(notAvailable)")
            // Check date should be the same as mock
            information = sut.getRocketInfo(launch: firstLaunch)
            XCTAssertEqual(information, "\(firstLaunch!.rocket.rocket_name) / \(firstLaunch!.rocket.rocket_type)")
        }
    }

    func testGetLaunchImage() {
        let launchRepository = LaunchRepositoryMock()
        let container = setUpContainer(launchRepository)
        if let sut = container.resolve(LaunchService.self) {
            let expectation = expectation(description: "should return rocket image")
            sut.getLaunchImage(launch: nil) { result in
                switch result {
                    case let .success(image):
                        let rocket = UIImage(named: "rocket")
                        XCTAssertEqual(image.size, rocket?.size ?? CGSize(width: 0.0, height: 0.0))
                        expectation.fulfill()
                    case .failure:
                        XCTAssert(false)
                }
            }

            waitForExpectations(timeout: 3)
        }
    }
}
