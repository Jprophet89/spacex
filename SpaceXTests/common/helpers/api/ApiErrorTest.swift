//
//  ApiErrorTest.swift
//  ApiErrorTest
//
//  Created by Joao Fonseca on 17/08/2021.
//

@testable import SpaceX
import XCTest

class ApiErrorTest: XCTestCase {
    override func setUp() {
        super.setUp()
    }

    override func tearDown() {
        super.tearDown()
    }

    func testShouldReturnCorrectErrorFromCode() {
        XCTAssertEqual(ApiError.fromCode(code: 200), ApiError.badData)
        XCTAssertEqual(ApiError.fromCode(code: 400), ApiError.badRequest)
        XCTAssertEqual(ApiError.fromCode(code: 401), ApiError.unauthorized)
        XCTAssertEqual(ApiError.fromCode(code: 403), ApiError.forbidden)
        XCTAssertEqual(ApiError.fromCode(code: 404), ApiError.notFound)
        XCTAssertEqual(ApiError.fromCode(code: 408), ApiError.timeout)
        XCTAssertEqual(ApiError.fromCode(code: 500), ApiError.serverError)
        XCTAssertEqual(ApiError.fromCode(code: 501), ApiError.unknown)
    }

    func testShouldReturnCorrectErrorMessage() {
        XCTAssertEqual(ApiError.getErrorMessage(code: 200), NSLocalizedString("ALERT_BAD_DATA", comment: ""))
        XCTAssertEqual(ApiError.getErrorMessage(code: 400), NSLocalizedString("ALERT_BAD_REQUEST", comment: ""))
        XCTAssertEqual(ApiError.getErrorMessage(code: 401), NSLocalizedString("ALERT_UNAUTHORIZED", comment: ""))
        XCTAssertEqual(ApiError.getErrorMessage(code: 403), NSLocalizedString("ALERT_FORBIDDEN", comment: ""))
        XCTAssertEqual(ApiError.getErrorMessage(code: 404), NSLocalizedString("ALERT_NOT_FOUND", comment: ""))
        XCTAssertEqual(ApiError.getErrorMessage(code: 408), NSLocalizedString("ALERT_TIMEOUT", comment: ""))
        XCTAssertEqual(ApiError.getErrorMessage(code: 500), NSLocalizedString("ALERT_SERVER_ERRROR", comment: ""))
        XCTAssertEqual(ApiError.getErrorMessage(code: 501), NSLocalizedString("ALERT_UNKNOWN", comment: ""))
    }
}
