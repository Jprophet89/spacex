// Generated using Sourcery 1.0.2 — https://github.com/krzysztofzablocki/Sourcery
// DO NOT EDIT

// Generated with SwiftyMocky 4.0.4

@testable import SpaceX
import Foundation
import RxSwift
import SwiftyMocky
import UIKit
import XCTest

// MARK: - CompanyRepository

open class CompanyRepositoryMock: CompanyRepository, Mock {
    public init(sequencing sequencingPolicy: SequencingPolicy = .lastWrittenResolvedFirst, stubbing stubbingPolicy: StubbingPolicy = .wrap, file: StaticString = #file, line: UInt = #line) {
        SwiftyMockyTestObserver.setup()
        self.sequencingPolicy = sequencingPolicy
        self.stubbingPolicy = stubbingPolicy
        self.file = file
        self.line = line
    }

    var matcher = Matcher.default
    var stubbingPolicy: StubbingPolicy = .wrap
    var sequencingPolicy: SequencingPolicy = .lastWrittenResolvedFirst

    private var queue = DispatchQueue(label: "com.swiftymocky.invocations", qos: .userInteractive)
    private var invocations: [MethodType] = []
    private var methodReturnValues: [Given] = []
    private var methodPerformValues: [Perform] = []
    private var file: StaticString?
    private var line: UInt?

    public typealias PropertyStub = Given
    public typealias MethodStub = Given
    public typealias SubscriptStub = Given

    /// Convenience method - call setupMock() to extend debug information when failure occurs
    public func setupMock(file: StaticString = #file, line: UInt = #line) {
        self.file = file
        self.line = line
    }

    /// Clear mock internals. You can specify what to reset (invocations aka verify, givens or performs) or leave it empty to clear all mock internals
    public func resetMock(_ scopes: MockScope...) {
        let scopes: [MockScope] = scopes.isEmpty ? [.invocation, .given, .perform] : scopes
        if scopes.contains(.invocation) { invocations = [] }
        if scopes.contains(.given) { methodReturnValues = [] }
        if scopes.contains(.perform) { methodPerformValues = [] }
    }

    open func getInfo() -> CompanyInfo? {
        addInvocation(.m_getInfo)
        let perform = methodPerformValue(.m_getInfo) as? () -> Void
        perform?()
        var __value: CompanyInfo?
        do {
            __value = try methodReturnValue(.m_getInfo).casted()
        } catch {
            // do nothing
        }
        return __value
    }

    open func refresh(completion: @escaping (Result<CompanyInfo, ApiError>) -> Void) {
        addInvocation(.m_refresh__completion_completion(Parameter<(Result<CompanyInfo, ApiError>) -> Void>.value(completion)))
        let perform = methodPerformValue(.m_refresh__completion_completion(Parameter<(Result<CompanyInfo, ApiError>) -> Void>.value(completion))) as? (@escaping (Result<CompanyInfo, ApiError>) -> Void) -> Void
        perform?(completion)
    }

    fileprivate enum MethodType {
        case m_getInfo
        case m_refresh__completion_completion(Parameter<(Result<CompanyInfo, ApiError>) -> Void>)

        static func compareParameters(lhs: MethodType, rhs: MethodType, matcher: Matcher) -> Matcher.ComparisonResult {
            switch (lhs, rhs) {
                case (.m_getInfo, .m_getInfo): return .match

                case let (.m_refresh__completion_completion(lhsCompletion), .m_refresh__completion_completion(rhsCompletion)):
                    var results: [Matcher.ParameterComparisonResult] = []
                    results.append(Matcher.ParameterComparisonResult(Parameter.compare(lhs: lhsCompletion, rhs: rhsCompletion, with: matcher), lhsCompletion, rhsCompletion, "completion"))
                    return Matcher.ComparisonResult(results)
                default: return .none
            }
        }

        func intValue() -> Int {
            switch self {
                case .m_getInfo: return 0
                case let .m_refresh__completion_completion(p0): return p0.intValue
            }
        }

        func assertionName() -> String {
            switch self {
                case .m_getInfo: return ".getInfo()"
                case .m_refresh__completion_completion: return ".refresh(completion:)"
            }
        }
    }

    open class Given: StubbedMethod {
        fileprivate var method: MethodType

        private init(method: MethodType, products: [StubProduct]) {
            self.method = method
            super.init(products)
        }

        public static func getInfo(willReturn: CompanyInfo?...) -> MethodStub {
            return Given(method: .m_getInfo, products: willReturn.map { StubProduct.return($0 as Any) })
        }

        public static func getInfo(willProduce: (Stubber<CompanyInfo?>) -> Void) -> MethodStub {
            let willReturn: [CompanyInfo?] = []
            let given: Given = { Given(method: .m_getInfo, products: willReturn.map { StubProduct.return($0 as Any) }) }()
            let stubber = given.stub(for: (CompanyInfo?).self)
            willProduce(stubber)
            return given
        }
    }

    public struct Verify {
        fileprivate var method: MethodType

        public static func getInfo() -> Verify { return Verify(method: .m_getInfo) }
        public static func refresh(completion: Parameter<(Result<CompanyInfo, ApiError>) -> Void>) -> Verify { return Verify(method: .m_refresh__completion_completion(completion)) }
    }

    public struct Perform {
        fileprivate var method: MethodType
        var performs: Any

        public static func getInfo(perform: @escaping () -> Void) -> Perform {
            return Perform(method: .m_getInfo, performs: perform)
        }

        public static func refresh(completion: Parameter<(Result<CompanyInfo, ApiError>) -> Void>, perform: @escaping (@escaping (Result<CompanyInfo, ApiError>) -> Void) -> Void) -> Perform {
            return Perform(method: .m_refresh__completion_completion(completion), performs: perform)
        }
    }

    public func given(_ method: Given) {
        methodReturnValues.append(method)
    }

    public func perform(_ method: Perform) {
        methodPerformValues.append(method)
        methodPerformValues.sort { $0.method.intValue() < $1.method.intValue() }
    }

    public func verify(_ method: Verify, count: Count = Count.moreOrEqual(to: 1), file: StaticString = #file, line: UInt = #line) {
        let fullMatches = matchingCalls(method, file: file, line: line)
        let success = count.matches(fullMatches)
        let assertionName = method.method.assertionName()
        let feedback: String = {
            guard !success else { return "" }
            return Utils.closestCallsMessage(
                for: self.invocations.map { invocation in
                    matcher.set(file: file, line: line)
                    defer { matcher.clearFileAndLine() }
                    return MethodType.compareParameters(lhs: invocation, rhs: method.method, matcher: matcher)
                },
                name: assertionName)
        }()
        MockyAssert(success, "Expected: \(count) invocations of `\(assertionName)`, but was: \(fullMatches).\(feedback)", file: file, line: line)
    }

    private func addInvocation(_ call: MethodType) {
        queue.sync { invocations.append(call) }
    }

    private func methodReturnValue(_ method: MethodType) throws -> StubProduct {
        matcher.set(file: file, line: line)
        defer { matcher.clearFileAndLine() }
        let candidates = sequencingPolicy.sorted(methodReturnValues, by: { $0.method.intValue() > $1.method.intValue() })
        let matched = candidates.first(where: { $0.isValid && MethodType.compareParameters(lhs: $0.method, rhs: method, matcher: matcher).isFullMatch })
        guard let product = matched?.getProduct(policy: stubbingPolicy) else { throw MockError.notStubed }
        return product
    }

    private func methodPerformValue(_ method: MethodType) -> Any? {
        matcher.set(file: file, line: line)
        defer { matcher.clearFileAndLine() }
        let matched = methodPerformValues.reversed().first { MethodType.compareParameters(lhs: $0.method, rhs: method, matcher: matcher).isFullMatch }
        return matched?.performs
    }

    private func matchingCalls(_ method: MethodType, file: StaticString?, line: UInt?) -> [MethodType] {
        matcher.set(file: file ?? self.file, line: line ?? self.line)
        defer { matcher.clearFileAndLine() }
        return invocations.filter { MethodType.compareParameters(lhs: $0, rhs: method, matcher: matcher).isFullMatch }
    }

    private func matchingCalls(_ method: Verify, file: StaticString?, line: UInt?) -> Int {
        return matchingCalls(method.method, file: file, line: line).count
    }

    private func givenGetterValue<T>(_ method: MethodType, _ message: String) -> T {
        do {
            return try methodReturnValue(method).casted()
        } catch {
            onFatalFailure(message)
            Failure(message)
        }
    }

    private func optionalGivenGetterValue<T>(_ method: MethodType, _: String) -> T? {
        do {
            return try methodReturnValue(method).casted()
        } catch {
            return nil
        }
    }

    private func onFatalFailure(_ message: String) {
        guard let file = self.file, let line = self.line else { return } // Let if fail if cannot handle gratefully
        SwiftyMockyTestObserver.handleFatalError(message: message, file: file, line: line)
    }
}

// MARK: - CompanyService

open class CompanyServiceMock: CompanyService, Mock {
    public init(sequencing sequencingPolicy: SequencingPolicy = .lastWrittenResolvedFirst, stubbing stubbingPolicy: StubbingPolicy = .wrap, file: StaticString = #file, line: UInt = #line) {
        SwiftyMockyTestObserver.setup()
        self.sequencingPolicy = sequencingPolicy
        self.stubbingPolicy = stubbingPolicy
        self.file = file
        self.line = line
    }

    var matcher = Matcher.default
    var stubbingPolicy: StubbingPolicy = .wrap
    var sequencingPolicy: SequencingPolicy = .lastWrittenResolvedFirst

    private var queue = DispatchQueue(label: "com.swiftymocky.invocations", qos: .userInteractive)
    private var invocations: [MethodType] = []
    private var methodReturnValues: [Given] = []
    private var methodPerformValues: [Perform] = []
    private var file: StaticString?
    private var line: UInt?

    public typealias PropertyStub = Given
    public typealias MethodStub = Given
    public typealias SubscriptStub = Given

    /// Convenience method - call setupMock() to extend debug information when failure occurs
    public func setupMock(file: StaticString = #file, line: UInt = #line) {
        self.file = file
        self.line = line
    }

    /// Clear mock internals. You can specify what to reset (invocations aka verify, givens or performs) or leave it empty to clear all mock internals
    public func resetMock(_ scopes: MockScope...) {
        let scopes: [MockScope] = scopes.isEmpty ? [.invocation, .given, .perform] : scopes
        if scopes.contains(.invocation) { invocations = [] }
        if scopes.contains(.given) { methodReturnValues = [] }
        if scopes.contains(.perform) { methodPerformValues = [] }
    }

    open func get() -> CompanyInfo? {
        addInvocation(.m_get)
        let perform = methodPerformValue(.m_get) as? () -> Void
        perform?()
        var __value: CompanyInfo?
        do {
            __value = try methodReturnValue(.m_get).casted()
        } catch {
            // do nothing
        }
        return __value
    }

    open func refresh(completion: @escaping (Result<CompanyInfo, ApiError>) -> Void) {
        addInvocation(.m_refresh__completion_completion(Parameter<(Result<CompanyInfo, ApiError>) -> Void>.value(completion)))
        let perform = methodPerformValue(.m_refresh__completion_completion(Parameter<(Result<CompanyInfo, ApiError>) -> Void>.value(completion))) as? (@escaping (Result<CompanyInfo, ApiError>) -> Void) -> Void
        perform?(completion)
    }

    fileprivate enum MethodType {
        case m_get
        case m_refresh__completion_completion(Parameter<(Result<CompanyInfo, ApiError>) -> Void>)

        static func compareParameters(lhs: MethodType, rhs: MethodType, matcher: Matcher) -> Matcher.ComparisonResult {
            switch (lhs, rhs) {
                case (.m_get, .m_get): return .match

                case let (.m_refresh__completion_completion(lhsCompletion), .m_refresh__completion_completion(rhsCompletion)):
                    var results: [Matcher.ParameterComparisonResult] = []
                    results.append(Matcher.ParameterComparisonResult(Parameter.compare(lhs: lhsCompletion, rhs: rhsCompletion, with: matcher), lhsCompletion, rhsCompletion, "completion"))
                    return Matcher.ComparisonResult(results)
                default: return .none
            }
        }

        func intValue() -> Int {
            switch self {
                case .m_get: return 0
                case let .m_refresh__completion_completion(p0): return p0.intValue
            }
        }

        func assertionName() -> String {
            switch self {
                case .m_get: return ".get()"
                case .m_refresh__completion_completion: return ".refresh(completion:)"
            }
        }
    }

    open class Given: StubbedMethod {
        fileprivate var method: MethodType

        private init(method: MethodType, products: [StubProduct]) {
            self.method = method
            super.init(products)
        }

        public static func get(willReturn: CompanyInfo?...) -> MethodStub {
            return Given(method: .m_get, products: willReturn.map { StubProduct.return($0 as Any) })
        }

        public static func get(willProduce: (Stubber<CompanyInfo?>) -> Void) -> MethodStub {
            let willReturn: [CompanyInfo?] = []
            let given: Given = { Given(method: .m_get, products: willReturn.map { StubProduct.return($0 as Any) }) }()
            let stubber = given.stub(for: (CompanyInfo?).self)
            willProduce(stubber)
            return given
        }
    }

    public struct Verify {
        fileprivate var method: MethodType

        public static func get() -> Verify { return Verify(method: .m_get) }
        public static func refresh(completion: Parameter<(Result<CompanyInfo, ApiError>) -> Void>) -> Verify { return Verify(method: .m_refresh__completion_completion(completion)) }
    }

    public struct Perform {
        fileprivate var method: MethodType
        var performs: Any

        public static func get(perform: @escaping () -> Void) -> Perform {
            return Perform(method: .m_get, performs: perform)
        }

        public static func refresh(completion: Parameter<(Result<CompanyInfo, ApiError>) -> Void>, perform: @escaping (@escaping (Result<CompanyInfo, ApiError>) -> Void) -> Void) -> Perform {
            return Perform(method: .m_refresh__completion_completion(completion), performs: perform)
        }
    }

    public func given(_ method: Given) {
        methodReturnValues.append(method)
    }

    public func perform(_ method: Perform) {
        methodPerformValues.append(method)
        methodPerformValues.sort { $0.method.intValue() < $1.method.intValue() }
    }

    public func verify(_ method: Verify, count: Count = Count.moreOrEqual(to: 1), file: StaticString = #file, line: UInt = #line) {
        let fullMatches = matchingCalls(method, file: file, line: line)
        let success = count.matches(fullMatches)
        let assertionName = method.method.assertionName()
        let feedback: String = {
            guard !success else { return "" }
            return Utils.closestCallsMessage(
                for: self.invocations.map { invocation in
                    matcher.set(file: file, line: line)
                    defer { matcher.clearFileAndLine() }
                    return MethodType.compareParameters(lhs: invocation, rhs: method.method, matcher: matcher)
                },
                name: assertionName)
        }()
        MockyAssert(success, "Expected: \(count) invocations of `\(assertionName)`, but was: \(fullMatches).\(feedback)", file: file, line: line)
    }

    private func addInvocation(_ call: MethodType) {
        queue.sync { invocations.append(call) }
    }

    private func methodReturnValue(_ method: MethodType) throws -> StubProduct {
        matcher.set(file: file, line: line)
        defer { matcher.clearFileAndLine() }
        let candidates = sequencingPolicy.sorted(methodReturnValues, by: { $0.method.intValue() > $1.method.intValue() })
        let matched = candidates.first(where: { $0.isValid && MethodType.compareParameters(lhs: $0.method, rhs: method, matcher: matcher).isFullMatch })
        guard let product = matched?.getProduct(policy: stubbingPolicy) else { throw MockError.notStubed }
        return product
    }

    private func methodPerformValue(_ method: MethodType) -> Any? {
        matcher.set(file: file, line: line)
        defer { matcher.clearFileAndLine() }
        let matched = methodPerformValues.reversed().first { MethodType.compareParameters(lhs: $0.method, rhs: method, matcher: matcher).isFullMatch }
        return matched?.performs
    }

    private func matchingCalls(_ method: MethodType, file: StaticString?, line: UInt?) -> [MethodType] {
        matcher.set(file: file ?? self.file, line: line ?? self.line)
        defer { matcher.clearFileAndLine() }
        return invocations.filter { MethodType.compareParameters(lhs: $0, rhs: method, matcher: matcher).isFullMatch }
    }

    private func matchingCalls(_ method: Verify, file: StaticString?, line: UInt?) -> Int {
        return matchingCalls(method.method, file: file, line: line).count
    }

    private func givenGetterValue<T>(_ method: MethodType, _ message: String) -> T {
        do {
            return try methodReturnValue(method).casted()
        } catch {
            onFatalFailure(message)
            Failure(message)
        }
    }

    private func optionalGivenGetterValue<T>(_ method: MethodType, _: String) -> T? {
        do {
            return try methodReturnValue(method).casted()
        } catch {
            return nil
        }
    }

    private func onFatalFailure(_ message: String) {
        guard let file = self.file, let line = self.line else { return } // Let if fail if cannot handle gratefully
        SwiftyMockyTestObserver.handleFatalError(message: message, file: file, line: line)
    }
}

// MARK: - LaunchDetailViewCoordinator

open class LaunchDetailViewCoordinatorMock: LaunchDetailViewCoordinator, Mock {
    public init(sequencing sequencingPolicy: SequencingPolicy = .lastWrittenResolvedFirst, stubbing stubbingPolicy: StubbingPolicy = .wrap, file: StaticString = #file, line: UInt = #line) {
        SwiftyMockyTestObserver.setup()
        self.sequencingPolicy = sequencingPolicy
        self.stubbingPolicy = stubbingPolicy
        self.file = file
        self.line = line
    }

    var matcher = Matcher.default
    var stubbingPolicy: StubbingPolicy = .wrap
    var sequencingPolicy: SequencingPolicy = .lastWrittenResolvedFirst

    private var queue = DispatchQueue(label: "com.swiftymocky.invocations", qos: .userInteractive)
    private var invocations: [MethodType] = []
    private var methodReturnValues: [Given] = []
    private var methodPerformValues: [Perform] = []
    private var file: StaticString?
    private var line: UInt?

    public typealias PropertyStub = Given
    public typealias MethodStub = Given
    public typealias SubscriptStub = Given

    /// Convenience method - call setupMock() to extend debug information when failure occurs
    public func setupMock(file: StaticString = #file, line: UInt = #line) {
        self.file = file
        self.line = line
    }

    /// Clear mock internals. You can specify what to reset (invocations aka verify, givens or performs) or leave it empty to clear all mock internals
    public func resetMock(_ scopes: MockScope...) {
        let scopes: [MockScope] = scopes.isEmpty ? [.invocation, .given, .perform] : scopes
        if scopes.contains(.invocation) { invocations = [] }
        if scopes.contains(.given) { methodReturnValues = [] }
        if scopes.contains(.perform) { methodPerformValues = [] }
    }

    open func openExternalLink(url: URL) {
        addInvocation(.m_openExternalLink__url_url(Parameter<URL>.value(url)))
        let perform = methodPerformValue(.m_openExternalLink__url_url(Parameter<URL>.value(url))) as? (URL) -> Void
        perform?(url)
    }

    fileprivate enum MethodType {
        case m_openExternalLink__url_url(Parameter<URL>)

        static func compareParameters(lhs: MethodType, rhs: MethodType, matcher: Matcher) -> Matcher.ComparisonResult {
            switch (lhs, rhs) {
                case let (.m_openExternalLink__url_url(lhsUrl), .m_openExternalLink__url_url(rhsUrl)):
                    var results: [Matcher.ParameterComparisonResult] = []
                    results.append(Matcher.ParameterComparisonResult(Parameter.compare(lhs: lhsUrl, rhs: rhsUrl, with: matcher), lhsUrl, rhsUrl, "url"))
                    return Matcher.ComparisonResult(results)
            }
        }

        func intValue() -> Int {
            switch self {
                case let .m_openExternalLink__url_url(p0): return p0.intValue
            }
        }

        func assertionName() -> String {
            switch self {
                case .m_openExternalLink__url_url: return ".openExternalLink(url:)"
            }
        }
    }

    open class Given: StubbedMethod {
        fileprivate var method: MethodType

        private init(method: MethodType, products: [StubProduct]) {
            self.method = method
            super.init(products)
        }
    }

    public struct Verify {
        fileprivate var method: MethodType

        public static func openExternalLink(url: Parameter<URL>) -> Verify { return Verify(method: .m_openExternalLink__url_url(url)) }
    }

    public struct Perform {
        fileprivate var method: MethodType
        var performs: Any

        public static func openExternalLink(url: Parameter<URL>, perform: @escaping (URL) -> Void) -> Perform {
            return Perform(method: .m_openExternalLink__url_url(url), performs: perform)
        }
    }

    public func given(_ method: Given) {
        methodReturnValues.append(method)
    }

    public func perform(_ method: Perform) {
        methodPerformValues.append(method)
        methodPerformValues.sort { $0.method.intValue() < $1.method.intValue() }
    }

    public func verify(_ method: Verify, count: Count = Count.moreOrEqual(to: 1), file: StaticString = #file, line: UInt = #line) {
        let fullMatches = matchingCalls(method, file: file, line: line)
        let success = count.matches(fullMatches)
        let assertionName = method.method.assertionName()
        let feedback: String = {
            guard !success else { return "" }
            return Utils.closestCallsMessage(
                for: self.invocations.map { invocation in
                    matcher.set(file: file, line: line)
                    defer { matcher.clearFileAndLine() }
                    return MethodType.compareParameters(lhs: invocation, rhs: method.method, matcher: matcher)
                },
                name: assertionName)
        }()
        MockyAssert(success, "Expected: \(count) invocations of `\(assertionName)`, but was: \(fullMatches).\(feedback)", file: file, line: line)
    }

    private func addInvocation(_ call: MethodType) {
        queue.sync { invocations.append(call) }
    }

    private func methodReturnValue(_ method: MethodType) throws -> StubProduct {
        matcher.set(file: file, line: line)
        defer { matcher.clearFileAndLine() }
        let candidates = sequencingPolicy.sorted(methodReturnValues, by: { $0.method.intValue() > $1.method.intValue() })
        let matched = candidates.first(where: { $0.isValid && MethodType.compareParameters(lhs: $0.method, rhs: method, matcher: matcher).isFullMatch })
        guard let product = matched?.getProduct(policy: stubbingPolicy) else { throw MockError.notStubed }
        return product
    }

    private func methodPerformValue(_ method: MethodType) -> Any? {
        matcher.set(file: file, line: line)
        defer { matcher.clearFileAndLine() }
        let matched = methodPerformValues.reversed().first { MethodType.compareParameters(lhs: $0.method, rhs: method, matcher: matcher).isFullMatch }
        return matched?.performs
    }

    private func matchingCalls(_ method: MethodType, file: StaticString?, line: UInt?) -> [MethodType] {
        matcher.set(file: file ?? self.file, line: line ?? self.line)
        defer { matcher.clearFileAndLine() }
        return invocations.filter { MethodType.compareParameters(lhs: $0, rhs: method, matcher: matcher).isFullMatch }
    }

    private func matchingCalls(_ method: Verify, file: StaticString?, line: UInt?) -> Int {
        return matchingCalls(method.method, file: file, line: line).count
    }

    private func givenGetterValue<T>(_ method: MethodType, _ message: String) -> T {
        do {
            return try methodReturnValue(method).casted()
        } catch {
            onFatalFailure(message)
            Failure(message)
        }
    }

    private func optionalGivenGetterValue<T>(_ method: MethodType, _: String) -> T? {
        do {
            return try methodReturnValue(method).casted()
        } catch {
            return nil
        }
    }

    private func onFatalFailure(_ message: String) {
        guard let file = self.file, let line = self.line else { return } // Let if fail if cannot handle gratefully
        SwiftyMockyTestObserver.handleFatalError(message: message, file: file, line: line)
    }
}

// MARK: - LaunchDetailViewModel

open class LaunchDetailViewModelMock: LaunchDetailViewModel, Mock {
    public init(sequencing sequencingPolicy: SequencingPolicy = .lastWrittenResolvedFirst, stubbing stubbingPolicy: StubbingPolicy = .wrap, file: StaticString = #file, line: UInt = #line) {
        SwiftyMockyTestObserver.setup()
        self.sequencingPolicy = sequencingPolicy
        self.stubbingPolicy = stubbingPolicy
        self.file = file
        self.line = line
    }

    var matcher = Matcher.default
    var stubbingPolicy: StubbingPolicy = .wrap
    var sequencingPolicy: SequencingPolicy = .lastWrittenResolvedFirst

    private var queue = DispatchQueue(label: "com.swiftymocky.invocations", qos: .userInteractive)
    private var invocations: [MethodType] = []
    private var methodReturnValues: [Given] = []
    private var methodPerformValues: [Perform] = []
    private var file: StaticString?
    private var line: UInt?

    public typealias PropertyStub = Given
    public typealias MethodStub = Given
    public typealias SubscriptStub = Given

    /// Convenience method - call setupMock() to extend debug information when failure occurs
    public func setupMock(file: StaticString = #file, line: UInt = #line) {
        self.file = file
        self.line = line
    }

    /// Clear mock internals. You can specify what to reset (invocations aka verify, givens or performs) or leave it empty to clear all mock internals
    public func resetMock(_ scopes: MockScope...) {
        let scopes: [MockScope] = scopes.isEmpty ? [.invocation, .given, .perform] : scopes
        if scopes.contains(.invocation) { invocations = [] }
        if scopes.contains(.given) { methodReturnValues = [] }
        if scopes.contains(.perform) { methodPerformValues = [] }
    }

    public var launch: Launch? {
        get { invocations.append(.p_launch_get); return __p_launch ?? optionalGivenGetterValue(.p_launch_get, "LaunchDetailViewModelMock - stub value for launch was not defined") }
        set { invocations.append(.p_launch_set(.value(newValue))); __p_launch = newValue }
    }

    private var __p_launch: (Launch)?

    open func initialize(_ launch: Launch) {
        addInvocation(.m_initialize__launch(Parameter<Launch>.value(launch)))
        let perform = methodPerformValue(.m_initialize__launch(Parameter<Launch>.value(launch))) as? (Launch) -> Void
        perform?(launch)
    }

    fileprivate enum MethodType {
        case m_initialize__launch(Parameter<Launch>)
        case p_launch_get
        case p_launch_set(Parameter<Launch?>)

        static func compareParameters(lhs: MethodType, rhs: MethodType, matcher: Matcher) -> Matcher.ComparisonResult {
            switch (lhs, rhs) {
                case let (.m_initialize__launch(lhsLaunch), .m_initialize__launch(rhsLaunch)):
                    var results: [Matcher.ParameterComparisonResult] = []
                    results.append(Matcher.ParameterComparisonResult(Parameter.compare(lhs: lhsLaunch, rhs: rhsLaunch, with: matcher), lhsLaunch, rhsLaunch, "_ launch"))
                    return Matcher.ComparisonResult(results)
                case (.p_launch_get, .p_launch_get): return Matcher.ComparisonResult.match
                case let (.p_launch_set(left), .p_launch_set(right)): return Matcher.ComparisonResult([Matcher.ParameterComparisonResult(Parameter<Launch?>.compare(lhs: left, rhs: right, with: matcher), left, right, "newValue")])
                default: return .none
            }
        }

        func intValue() -> Int {
            switch self {
                case let .m_initialize__launch(p0): return p0.intValue
                case .p_launch_get: return 0
                case let .p_launch_set(newValue): return newValue.intValue
            }
        }

        func assertionName() -> String {
            switch self {
                case .m_initialize__launch: return ".initialize(_:)"
                case .p_launch_get: return "[get] .launch"
                case .p_launch_set: return "[set] .launch"
            }
        }
    }

    open class Given: StubbedMethod {
        fileprivate var method: MethodType

        private init(method: MethodType, products: [StubProduct]) {
            self.method = method
            super.init(products)
        }

        public static func launch(getter defaultValue: Launch?...) -> PropertyStub {
            return Given(method: .p_launch_get, products: defaultValue.map { StubProduct.return($0 as Any) })
        }
    }

    public struct Verify {
        fileprivate var method: MethodType

        public static func initialize(_ launch: Parameter<Launch>) -> Verify { return Verify(method: .m_initialize__launch(launch)) }
        public static var launch: Verify { return Verify(method: .p_launch_get) }
        public static func launch(set newValue: Parameter<Launch?>) -> Verify { return Verify(method: .p_launch_set(newValue)) }
    }

    public struct Perform {
        fileprivate var method: MethodType
        var performs: Any

        public static func initialize(_ launch: Parameter<Launch>, perform: @escaping (Launch) -> Void) -> Perform {
            return Perform(method: .m_initialize__launch(launch), performs: perform)
        }
    }

    public func given(_ method: Given) {
        methodReturnValues.append(method)
    }

    public func perform(_ method: Perform) {
        methodPerformValues.append(method)
        methodPerformValues.sort { $0.method.intValue() < $1.method.intValue() }
    }

    public func verify(_ method: Verify, count: Count = Count.moreOrEqual(to: 1), file: StaticString = #file, line: UInt = #line) {
        let fullMatches = matchingCalls(method, file: file, line: line)
        let success = count.matches(fullMatches)
        let assertionName = method.method.assertionName()
        let feedback: String = {
            guard !success else { return "" }
            return Utils.closestCallsMessage(
                for: self.invocations.map { invocation in
                    matcher.set(file: file, line: line)
                    defer { matcher.clearFileAndLine() }
                    return MethodType.compareParameters(lhs: invocation, rhs: method.method, matcher: matcher)
                },
                name: assertionName)
        }()
        MockyAssert(success, "Expected: \(count) invocations of `\(assertionName)`, but was: \(fullMatches).\(feedback)", file: file, line: line)
    }

    private func addInvocation(_ call: MethodType) {
        queue.sync { invocations.append(call) }
    }

    private func methodReturnValue(_ method: MethodType) throws -> StubProduct {
        matcher.set(file: file, line: line)
        defer { matcher.clearFileAndLine() }
        let candidates = sequencingPolicy.sorted(methodReturnValues, by: { $0.method.intValue() > $1.method.intValue() })
        let matched = candidates.first(where: { $0.isValid && MethodType.compareParameters(lhs: $0.method, rhs: method, matcher: matcher).isFullMatch })
        guard let product = matched?.getProduct(policy: stubbingPolicy) else { throw MockError.notStubed }
        return product
    }

    private func methodPerformValue(_ method: MethodType) -> Any? {
        matcher.set(file: file, line: line)
        defer { matcher.clearFileAndLine() }
        let matched = methodPerformValues.reversed().first { MethodType.compareParameters(lhs: $0.method, rhs: method, matcher: matcher).isFullMatch }
        return matched?.performs
    }

    private func matchingCalls(_ method: MethodType, file: StaticString?, line: UInt?) -> [MethodType] {
        matcher.set(file: file ?? self.file, line: line ?? self.line)
        defer { matcher.clearFileAndLine() }
        return invocations.filter { MethodType.compareParameters(lhs: $0, rhs: method, matcher: matcher).isFullMatch }
    }

    private func matchingCalls(_ method: Verify, file: StaticString?, line: UInt?) -> Int {
        return matchingCalls(method.method, file: file, line: line).count
    }

    private func givenGetterValue<T>(_ method: MethodType, _ message: String) -> T {
        do {
            return try methodReturnValue(method).casted()
        } catch {
            onFatalFailure(message)
            Failure(message)
        }
    }

    private func optionalGivenGetterValue<T>(_ method: MethodType, _: String) -> T? {
        do {
            return try methodReturnValue(method).casted()
        } catch {
            return nil
        }
    }

    private func onFatalFailure(_ message: String) {
        guard let file = self.file, let line = self.line else { return } // Let if fail if cannot handle gratefully
        SwiftyMockyTestObserver.handleFatalError(message: message, file: file, line: line)
    }
}

// MARK: - LaunchRepository

open class LaunchRepositoryMock: LaunchRepository, Mock {
    public init(sequencing sequencingPolicy: SequencingPolicy = .lastWrittenResolvedFirst, stubbing stubbingPolicy: StubbingPolicy = .wrap, file: StaticString = #file, line: UInt = #line) {
        SwiftyMockyTestObserver.setup()
        self.sequencingPolicy = sequencingPolicy
        self.stubbingPolicy = stubbingPolicy
        self.file = file
        self.line = line
    }

    var matcher = Matcher.default
    var stubbingPolicy: StubbingPolicy = .wrap
    var sequencingPolicy: SequencingPolicy = .lastWrittenResolvedFirst

    private var queue = DispatchQueue(label: "com.swiftymocky.invocations", qos: .userInteractive)
    private var invocations: [MethodType] = []
    private var methodReturnValues: [Given] = []
    private var methodPerformValues: [Perform] = []
    private var file: StaticString?
    private var line: UInt?

    public typealias PropertyStub = Given
    public typealias MethodStub = Given
    public typealias SubscriptStub = Given

    /// Convenience method - call setupMock() to extend debug information when failure occurs
    public func setupMock(file: StaticString = #file, line: UInt = #line) {
        self.file = file
        self.line = line
    }

    /// Clear mock internals. You can specify what to reset (invocations aka verify, givens or performs) or leave it empty to clear all mock internals
    public func resetMock(_ scopes: MockScope...) {
        let scopes: [MockScope] = scopes.isEmpty ? [.invocation, .given, .perform] : scopes
        if scopes.contains(.invocation) { invocations = [] }
        if scopes.contains(.given) { methodReturnValues = [] }
        if scopes.contains(.perform) { methodPerformValues = [] }
    }

    open func get() -> [Launch?]? {
        addInvocation(.m_get)
        let perform = methodPerformValue(.m_get) as? () -> Void
        perform?()
        var __value: [Launch?]?
        do {
            __value = try methodReturnValue(.m_get).casted()
        } catch {
            // do nothing
        }
        return __value
    }

    open func refresh(filter: Filter?, completion: @escaping (Result<[Launch?], ApiError>) -> Void) {
        addInvocation(.m_refresh__filter_filtercompletion_completion(Parameter<Filter?>.value(filter), Parameter<(Result<[Launch?], ApiError>) -> Void>.value(completion)))
        let perform = methodPerformValue(.m_refresh__filter_filtercompletion_completion(Parameter<Filter?>.value(filter), Parameter<(Result<[Launch?], ApiError>) -> Void>.value(completion))) as? (Filter?, @escaping (Result<[Launch?], ApiError>) -> Void) -> Void
        perform?(filter, completion)
    }

    fileprivate enum MethodType {
        case m_get
        case m_refresh__filter_filtercompletion_completion(Parameter<Filter?>, Parameter<(Result<[Launch?], ApiError>) -> Void>)

        static func compareParameters(lhs: MethodType, rhs: MethodType, matcher: Matcher) -> Matcher.ComparisonResult {
            switch (lhs, rhs) {
                case (.m_get, .m_get): return .match

                case let (.m_refresh__filter_filtercompletion_completion(lhsFilter, lhsCompletion), .m_refresh__filter_filtercompletion_completion(rhsFilter, rhsCompletion)):
                    var results: [Matcher.ParameterComparisonResult] = []
                    results.append(Matcher.ParameterComparisonResult(Parameter.compare(lhs: lhsFilter, rhs: rhsFilter, with: matcher), lhsFilter, rhsFilter, "filter"))
                    results.append(Matcher.ParameterComparisonResult(Parameter.compare(lhs: lhsCompletion, rhs: rhsCompletion, with: matcher), lhsCompletion, rhsCompletion, "completion"))
                    return Matcher.ComparisonResult(results)
                default: return .none
            }
        }

        func intValue() -> Int {
            switch self {
                case .m_get: return 0
                case let .m_refresh__filter_filtercompletion_completion(p0, p1): return p0.intValue + p1.intValue
            }
        }

        func assertionName() -> String {
            switch self {
                case .m_get: return ".get()"
                case .m_refresh__filter_filtercompletion_completion: return ".refresh(filter:completion:)"
            }
        }
    }

    open class Given: StubbedMethod {
        fileprivate var method: MethodType

        private init(method: MethodType, products: [StubProduct]) {
            self.method = method
            super.init(products)
        }

        public static func get(willReturn: [Launch?]?...) -> MethodStub {
            return Given(method: .m_get, products: willReturn.map { StubProduct.return($0 as Any) })
        }

        public static func get(willProduce: (Stubber<[Launch?]?>) -> Void) -> MethodStub {
            let willReturn: [[Launch?]?] = []
            let given: Given = { Given(method: .m_get, products: willReturn.map { StubProduct.return($0 as Any) }) }()
            let stubber = given.stub(for: ([Launch?]?).self)
            willProduce(stubber)
            return given
        }
    }

    public struct Verify {
        fileprivate var method: MethodType

        public static func get() -> Verify { return Verify(method: .m_get) }
        public static func refresh(filter: Parameter<Filter?>, completion: Parameter<(Result<[Launch?], ApiError>) -> Void>) -> Verify { return Verify(method: .m_refresh__filter_filtercompletion_completion(filter, completion)) }
    }

    public struct Perform {
        fileprivate var method: MethodType
        var performs: Any

        public static func get(perform: @escaping () -> Void) -> Perform {
            return Perform(method: .m_get, performs: perform)
        }

        public static func refresh(filter: Parameter<Filter?>, completion: Parameter<(Result<[Launch?], ApiError>) -> Void>, perform: @escaping (Filter?, @escaping (Result<[Launch?], ApiError>) -> Void) -> Void) -> Perform {
            return Perform(method: .m_refresh__filter_filtercompletion_completion(filter, completion), performs: perform)
        }
    }

    public func given(_ method: Given) {
        methodReturnValues.append(method)
    }

    public func perform(_ method: Perform) {
        methodPerformValues.append(method)
        methodPerformValues.sort { $0.method.intValue() < $1.method.intValue() }
    }

    public func verify(_ method: Verify, count: Count = Count.moreOrEqual(to: 1), file: StaticString = #file, line: UInt = #line) {
        let fullMatches = matchingCalls(method, file: file, line: line)
        let success = count.matches(fullMatches)
        let assertionName = method.method.assertionName()
        let feedback: String = {
            guard !success else { return "" }
            return Utils.closestCallsMessage(
                for: self.invocations.map { invocation in
                    matcher.set(file: file, line: line)
                    defer { matcher.clearFileAndLine() }
                    return MethodType.compareParameters(lhs: invocation, rhs: method.method, matcher: matcher)
                },
                name: assertionName)
        }()
        MockyAssert(success, "Expected: \(count) invocations of `\(assertionName)`, but was: \(fullMatches).\(feedback)", file: file, line: line)
    }

    private func addInvocation(_ call: MethodType) {
        queue.sync { invocations.append(call) }
    }

    private func methodReturnValue(_ method: MethodType) throws -> StubProduct {
        matcher.set(file: file, line: line)
        defer { matcher.clearFileAndLine() }
        let candidates = sequencingPolicy.sorted(methodReturnValues, by: { $0.method.intValue() > $1.method.intValue() })
        let matched = candidates.first(where: { $0.isValid && MethodType.compareParameters(lhs: $0.method, rhs: method, matcher: matcher).isFullMatch })
        guard let product = matched?.getProduct(policy: stubbingPolicy) else { throw MockError.notStubed }
        return product
    }

    private func methodPerformValue(_ method: MethodType) -> Any? {
        matcher.set(file: file, line: line)
        defer { matcher.clearFileAndLine() }
        let matched = methodPerformValues.reversed().first { MethodType.compareParameters(lhs: $0.method, rhs: method, matcher: matcher).isFullMatch }
        return matched?.performs
    }

    private func matchingCalls(_ method: MethodType, file: StaticString?, line: UInt?) -> [MethodType] {
        matcher.set(file: file ?? self.file, line: line ?? self.line)
        defer { matcher.clearFileAndLine() }
        return invocations.filter { MethodType.compareParameters(lhs: $0, rhs: method, matcher: matcher).isFullMatch }
    }

    private func matchingCalls(_ method: Verify, file: StaticString?, line: UInt?) -> Int {
        return matchingCalls(method.method, file: file, line: line).count
    }

    private func givenGetterValue<T>(_ method: MethodType, _ message: String) -> T {
        do {
            return try methodReturnValue(method).casted()
        } catch {
            onFatalFailure(message)
            Failure(message)
        }
    }

    private func optionalGivenGetterValue<T>(_ method: MethodType, _: String) -> T? {
        do {
            return try methodReturnValue(method).casted()
        } catch {
            return nil
        }
    }

    private func onFatalFailure(_ message: String) {
        guard let file = self.file, let line = self.line else { return } // Let if fail if cannot handle gratefully
        SwiftyMockyTestObserver.handleFatalError(message: message, file: file, line: line)
    }
}

// MARK: - LaunchService

open class LaunchServiceMock: LaunchService, Mock {
    public init(sequencing sequencingPolicy: SequencingPolicy = .lastWrittenResolvedFirst, stubbing stubbingPolicy: StubbingPolicy = .wrap, file: StaticString = #file, line: UInt = #line) {
        SwiftyMockyTestObserver.setup()
        self.sequencingPolicy = sequencingPolicy
        self.stubbingPolicy = stubbingPolicy
        self.file = file
        self.line = line
    }

    var matcher = Matcher.default
    var stubbingPolicy: StubbingPolicy = .wrap
    var sequencingPolicy: SequencingPolicy = .lastWrittenResolvedFirst

    private var queue = DispatchQueue(label: "com.swiftymocky.invocations", qos: .userInteractive)
    private var invocations: [MethodType] = []
    private var methodReturnValues: [Given] = []
    private var methodPerformValues: [Perform] = []
    private var file: StaticString?
    private var line: UInt?

    public typealias PropertyStub = Given
    public typealias MethodStub = Given
    public typealias SubscriptStub = Given

    /// Convenience method - call setupMock() to extend debug information when failure occurs
    public func setupMock(file: StaticString = #file, line: UInt = #line) {
        self.file = file
        self.line = line
    }

    /// Clear mock internals. You can specify what to reset (invocations aka verify, givens or performs) or leave it empty to clear all mock internals
    public func resetMock(_ scopes: MockScope...) {
        let scopes: [MockScope] = scopes.isEmpty ? [.invocation, .given, .perform] : scopes
        if scopes.contains(.invocation) { invocations = [] }
        if scopes.contains(.given) { methodReturnValues = [] }
        if scopes.contains(.perform) { methodPerformValues = [] }
    }

    open func get() -> [Launch?]? {
        addInvocation(.m_get)
        let perform = methodPerformValue(.m_get) as? () -> Void
        perform?()
        var __value: [Launch?]?
        do {
            __value = try methodReturnValue(.m_get).casted()
        } catch {
            // do nothing
        }
        return __value
    }

    open func refresh(filter: Filter?, completion: @escaping (Result<[Launch?], ApiError>) -> Void) {
        addInvocation(.m_refresh__filter_filtercompletion_completion(Parameter<Filter?>.value(filter), Parameter<(Result<[Launch?], ApiError>) -> Void>.value(completion)))
        let perform = methodPerformValue(.m_refresh__filter_filtercompletion_completion(Parameter<Filter?>.value(filter), Parameter<(Result<[Launch?], ApiError>) -> Void>.value(completion))) as? (Filter?, @escaping (Result<[Launch?], ApiError>) -> Void) -> Void
        perform?(filter, completion)
    }

    open func getDate(launch: Launch?) -> Date? {
        addInvocation(.m_getDate__launch_launch(Parameter<Launch?>.value(launch)))
        let perform = methodPerformValue(.m_getDate__launch_launch(Parameter<Launch?>.value(launch))) as? (Launch?) -> Void
        perform?(launch)
        var __value: Date?
        do {
            __value = try methodReturnValue(.m_getDate__launch_launch(Parameter<Launch?>.value(launch))).casted()
        } catch {
            // do nothing
        }
        return __value
    }

    open func getRocketInfo(launch: Launch?) -> String {
        addInvocation(.m_getRocketInfo__launch_launch(Parameter<Launch?>.value(launch)))
        let perform = methodPerformValue(.m_getRocketInfo__launch_launch(Parameter<Launch?>.value(launch))) as? (Launch?) -> Void
        perform?(launch)
        var __value: String
        do {
            __value = try methodReturnValue(.m_getRocketInfo__launch_launch(Parameter<Launch?>.value(launch))).casted()
        } catch {
            onFatalFailure("Stub return value not specified for getRocketInfo(launch: Launch?). Use given")
            Failure("Stub return value not specified for getRocketInfo(launch: Launch?). Use given")
        }
        return __value
    }

    open func getLaunchImage(launch: Launch?, completion: @escaping (Result<UIImage, ApiError>) -> Void) {
        addInvocation(.m_getLaunchImage__launch_launchcompletion_completion(Parameter<Launch?>.value(launch), Parameter<(Result<UIImage, ApiError>) -> Void>.value(completion)))
        let perform = methodPerformValue(.m_getLaunchImage__launch_launchcompletion_completion(Parameter<Launch?>.value(launch), Parameter<(Result<UIImage, ApiError>) -> Void>.value(completion))) as? (Launch?, @escaping (Result<UIImage, ApiError>) -> Void) -> Void
        perform?(launch, completion)
    }

    fileprivate enum MethodType {
        case m_get
        case m_refresh__filter_filtercompletion_completion(Parameter<Filter?>, Parameter<(Result<[Launch?], ApiError>) -> Void>)
        case m_getDate__launch_launch(Parameter<Launch?>)
        case m_getRocketInfo__launch_launch(Parameter<Launch?>)
        case m_getLaunchImage__launch_launchcompletion_completion(Parameter<Launch?>, Parameter<(Result<UIImage, ApiError>) -> Void>)

        static func compareParameters(lhs: MethodType, rhs: MethodType, matcher: Matcher) -> Matcher.ComparisonResult {
            switch (lhs, rhs) {
                case (.m_get, .m_get): return .match

                case let (.m_refresh__filter_filtercompletion_completion(lhsFilter, lhsCompletion), .m_refresh__filter_filtercompletion_completion(rhsFilter, rhsCompletion)):
                    var results: [Matcher.ParameterComparisonResult] = []
                    results.append(Matcher.ParameterComparisonResult(Parameter.compare(lhs: lhsFilter, rhs: rhsFilter, with: matcher), lhsFilter, rhsFilter, "filter"))
                    results.append(Matcher.ParameterComparisonResult(Parameter.compare(lhs: lhsCompletion, rhs: rhsCompletion, with: matcher), lhsCompletion, rhsCompletion, "completion"))
                    return Matcher.ComparisonResult(results)

                case let (.m_getDate__launch_launch(lhsLaunch), .m_getDate__launch_launch(rhsLaunch)):
                    var results: [Matcher.ParameterComparisonResult] = []
                    results.append(Matcher.ParameterComparisonResult(Parameter.compare(lhs: lhsLaunch, rhs: rhsLaunch, with: matcher), lhsLaunch, rhsLaunch, "launch"))
                    return Matcher.ComparisonResult(results)

                case let (.m_getRocketInfo__launch_launch(lhsLaunch), .m_getRocketInfo__launch_launch(rhsLaunch)):
                    var results: [Matcher.ParameterComparisonResult] = []
                    results.append(Matcher.ParameterComparisonResult(Parameter.compare(lhs: lhsLaunch, rhs: rhsLaunch, with: matcher), lhsLaunch, rhsLaunch, "launch"))
                    return Matcher.ComparisonResult(results)

                case let (.m_getLaunchImage__launch_launchcompletion_completion(lhsLaunch, lhsCompletion), .m_getLaunchImage__launch_launchcompletion_completion(rhsLaunch, rhsCompletion)):
                    var results: [Matcher.ParameterComparisonResult] = []
                    results.append(Matcher.ParameterComparisonResult(Parameter.compare(lhs: lhsLaunch, rhs: rhsLaunch, with: matcher), lhsLaunch, rhsLaunch, "launch"))
                    results.append(Matcher.ParameterComparisonResult(Parameter.compare(lhs: lhsCompletion, rhs: rhsCompletion, with: matcher), lhsCompletion, rhsCompletion, "completion"))
                    return Matcher.ComparisonResult(results)
                default: return .none
            }
        }

        func intValue() -> Int {
            switch self {
                case .m_get: return 0
                case let .m_refresh__filter_filtercompletion_completion(p0, p1): return p0.intValue + p1.intValue
                case let .m_getDate__launch_launch(p0): return p0.intValue
                case let .m_getRocketInfo__launch_launch(p0): return p0.intValue
                case let .m_getLaunchImage__launch_launchcompletion_completion(p0, p1): return p0.intValue + p1.intValue
            }
        }

        func assertionName() -> String {
            switch self {
                case .m_get: return ".get()"
                case .m_refresh__filter_filtercompletion_completion: return ".refresh(filter:completion:)"
                case .m_getDate__launch_launch: return ".getDate(launch:)"
                case .m_getRocketInfo__launch_launch: return ".getRocketInfo(launch:)"
                case .m_getLaunchImage__launch_launchcompletion_completion: return ".getLaunchImage(launch:completion:)"
            }
        }
    }

    open class Given: StubbedMethod {
        fileprivate var method: MethodType

        private init(method: MethodType, products: [StubProduct]) {
            self.method = method
            super.init(products)
        }

        public static func get(willReturn: [Launch?]?...) -> MethodStub {
            return Given(method: .m_get, products: willReturn.map { StubProduct.return($0 as Any) })
        }

        public static func getDate(launch: Parameter<Launch?>, willReturn: Date?...) -> MethodStub {
            return Given(method: .m_getDate__launch_launch(launch), products: willReturn.map { StubProduct.return($0 as Any) })
        }

        public static func getRocketInfo(launch: Parameter<Launch?>, willReturn: String...) -> MethodStub {
            return Given(method: .m_getRocketInfo__launch_launch(launch), products: willReturn.map { StubProduct.return($0 as Any) })
        }

        public static func get(willProduce: (Stubber<[Launch?]?>) -> Void) -> MethodStub {
            let willReturn: [[Launch?]?] = []
            let given: Given = { Given(method: .m_get, products: willReturn.map { StubProduct.return($0 as Any) }) }()
            let stubber = given.stub(for: ([Launch?]?).self)
            willProduce(stubber)
            return given
        }

        public static func getDate(launch: Parameter<Launch?>, willProduce: (Stubber<Date?>) -> Void) -> MethodStub {
            let willReturn: [Date?] = []
            let given: Given = { Given(method: .m_getDate__launch_launch(launch), products: willReturn.map { StubProduct.return($0 as Any) }) }()
            let stubber = given.stub(for: (Date?).self)
            willProduce(stubber)
            return given
        }

        public static func getRocketInfo(launch: Parameter<Launch?>, willProduce: (Stubber<String>) -> Void) -> MethodStub {
            let willReturn: [String] = []
            let given: Given = { Given(method: .m_getRocketInfo__launch_launch(launch), products: willReturn.map { StubProduct.return($0 as Any) }) }()
            let stubber = given.stub(for: String.self)
            willProduce(stubber)
            return given
        }
    }

    public struct Verify {
        fileprivate var method: MethodType

        public static func get() -> Verify { return Verify(method: .m_get) }
        public static func refresh(filter: Parameter<Filter?>, completion: Parameter<(Result<[Launch?], ApiError>) -> Void>) -> Verify { return Verify(method: .m_refresh__filter_filtercompletion_completion(filter, completion)) }
        public static func getDate(launch: Parameter<Launch?>) -> Verify { return Verify(method: .m_getDate__launch_launch(launch)) }
        public static func getRocketInfo(launch: Parameter<Launch?>) -> Verify { return Verify(method: .m_getRocketInfo__launch_launch(launch)) }
        public static func getLaunchImage(launch: Parameter<Launch?>, completion: Parameter<(Result<UIImage, ApiError>) -> Void>) -> Verify { return Verify(method: .m_getLaunchImage__launch_launchcompletion_completion(launch, completion)) }
    }

    public struct Perform {
        fileprivate var method: MethodType
        var performs: Any

        public static func get(perform: @escaping () -> Void) -> Perform {
            return Perform(method: .m_get, performs: perform)
        }

        public static func refresh(filter: Parameter<Filter?>, completion: Parameter<(Result<[Launch?], ApiError>) -> Void>, perform: @escaping (Filter?, @escaping (Result<[Launch?], ApiError>) -> Void) -> Void) -> Perform {
            return Perform(method: .m_refresh__filter_filtercompletion_completion(filter, completion), performs: perform)
        }

        public static func getDate(launch: Parameter<Launch?>, perform: @escaping (Launch?) -> Void) -> Perform {
            return Perform(method: .m_getDate__launch_launch(launch), performs: perform)
        }

        public static func getRocketInfo(launch: Parameter<Launch?>, perform: @escaping (Launch?) -> Void) -> Perform {
            return Perform(method: .m_getRocketInfo__launch_launch(launch), performs: perform)
        }

        public static func getLaunchImage(launch: Parameter<Launch?>, completion: Parameter<(Result<UIImage, ApiError>) -> Void>, perform: @escaping (Launch?, @escaping (Result<UIImage, ApiError>) -> Void) -> Void) -> Perform {
            return Perform(method: .m_getLaunchImage__launch_launchcompletion_completion(launch, completion), performs: perform)
        }
    }

    public func given(_ method: Given) {
        methodReturnValues.append(method)
    }

    public func perform(_ method: Perform) {
        methodPerformValues.append(method)
        methodPerformValues.sort { $0.method.intValue() < $1.method.intValue() }
    }

    public func verify(_ method: Verify, count: Count = Count.moreOrEqual(to: 1), file: StaticString = #file, line: UInt = #line) {
        let fullMatches = matchingCalls(method, file: file, line: line)
        let success = count.matches(fullMatches)
        let assertionName = method.method.assertionName()
        let feedback: String = {
            guard !success else { return "" }
            return Utils.closestCallsMessage(
                for: self.invocations.map { invocation in
                    matcher.set(file: file, line: line)
                    defer { matcher.clearFileAndLine() }
                    return MethodType.compareParameters(lhs: invocation, rhs: method.method, matcher: matcher)
                },
                name: assertionName)
        }()
        MockyAssert(success, "Expected: \(count) invocations of `\(assertionName)`, but was: \(fullMatches).\(feedback)", file: file, line: line)
    }

    private func addInvocation(_ call: MethodType) {
        queue.sync { invocations.append(call) }
    }

    private func methodReturnValue(_ method: MethodType) throws -> StubProduct {
        matcher.set(file: file, line: line)
        defer { matcher.clearFileAndLine() }
        let candidates = sequencingPolicy.sorted(methodReturnValues, by: { $0.method.intValue() > $1.method.intValue() })
        let matched = candidates.first(where: { $0.isValid && MethodType.compareParameters(lhs: $0.method, rhs: method, matcher: matcher).isFullMatch })
        guard let product = matched?.getProduct(policy: stubbingPolicy) else { throw MockError.notStubed }
        return product
    }

    private func methodPerformValue(_ method: MethodType) -> Any? {
        matcher.set(file: file, line: line)
        defer { matcher.clearFileAndLine() }
        let matched = methodPerformValues.reversed().first { MethodType.compareParameters(lhs: $0.method, rhs: method, matcher: matcher).isFullMatch }
        return matched?.performs
    }

    private func matchingCalls(_ method: MethodType, file: StaticString?, line: UInt?) -> [MethodType] {
        matcher.set(file: file ?? self.file, line: line ?? self.line)
        defer { matcher.clearFileAndLine() }
        return invocations.filter { MethodType.compareParameters(lhs: $0, rhs: method, matcher: matcher).isFullMatch }
    }

    private func matchingCalls(_ method: Verify, file: StaticString?, line: UInt?) -> Int {
        return matchingCalls(method.method, file: file, line: line).count
    }

    private func givenGetterValue<T>(_ method: MethodType, _ message: String) -> T {
        do {
            return try methodReturnValue(method).casted()
        } catch {
            onFatalFailure(message)
            Failure(message)
        }
    }

    private func optionalGivenGetterValue<T>(_ method: MethodType, _: String) -> T? {
        do {
            return try methodReturnValue(method).casted()
        } catch {
            return nil
        }
    }

    private func onFatalFailure(_ message: String) {
        guard let file = self.file, let line = self.line else { return } // Let if fail if cannot handle gratefully
        SwiftyMockyTestObserver.handleFatalError(message: message, file: file, line: line)
    }
}

// MARK: - MainViewCoordinator

open class MainViewCoordinatorMock: MainViewCoordinator, Mock {
    public init(sequencing sequencingPolicy: SequencingPolicy = .lastWrittenResolvedFirst, stubbing stubbingPolicy: StubbingPolicy = .wrap, file: StaticString = #file, line: UInt = #line) {
        SwiftyMockyTestObserver.setup()
        self.sequencingPolicy = sequencingPolicy
        self.stubbingPolicy = stubbingPolicy
        self.file = file
        self.line = line
    }

    var matcher = Matcher.default
    var stubbingPolicy: StubbingPolicy = .wrap
    var sequencingPolicy: SequencingPolicy = .lastWrittenResolvedFirst

    private var queue = DispatchQueue(label: "com.swiftymocky.invocations", qos: .userInteractive)
    private var invocations: [MethodType] = []
    private var methodReturnValues: [Given] = []
    private var methodPerformValues: [Perform] = []
    private var file: StaticString?
    private var line: UInt?

    public typealias PropertyStub = Given
    public typealias MethodStub = Given
    public typealias SubscriptStub = Given

    /// Convenience method - call setupMock() to extend debug information when failure occurs
    public func setupMock(file: StaticString = #file, line: UInt = #line) {
        self.file = file
        self.line = line
    }

    /// Clear mock internals. You can specify what to reset (invocations aka verify, givens or performs) or leave it empty to clear all mock internals
    public func resetMock(_ scopes: MockScope...) {
        let scopes: [MockScope] = scopes.isEmpty ? [.invocation, .given, .perform] : scopes
        if scopes.contains(.invocation) { invocations = [] }
        if scopes.contains(.given) { methodReturnValues = [] }
        if scopes.contains(.perform) { methodPerformValues = [] }
    }

    open func presentFilters(from: MainViewController) {
        addInvocation(.m_presentFilters__from_from(Parameter<MainViewController>.value(from)))
        let perform = methodPerformValue(.m_presentFilters__from_from(Parameter<MainViewController>.value(from))) as? (MainViewController) -> Void
        perform?(from)
    }

    open func presentLaunch(from: MainViewController, launch: Launch) {
        addInvocation(.m_presentLaunch__from_fromlaunch_launch(Parameter<MainViewController>.value(from), Parameter<Launch>.value(launch)))
        let perform = methodPerformValue(.m_presentLaunch__from_fromlaunch_launch(Parameter<MainViewController>.value(from), Parameter<Launch>.value(launch))) as? (MainViewController, Launch) -> Void
        perform?(from, launch)
    }

    fileprivate enum MethodType {
        case m_presentFilters__from_from(Parameter<MainViewController>)
        case m_presentLaunch__from_fromlaunch_launch(Parameter<MainViewController>, Parameter<Launch>)

        static func compareParameters(lhs: MethodType, rhs: MethodType, matcher: Matcher) -> Matcher.ComparisonResult {
            switch (lhs, rhs) {
                case let (.m_presentFilters__from_from(lhsFrom), .m_presentFilters__from_from(rhsFrom)):
                    var results: [Matcher.ParameterComparisonResult] = []
                    results.append(Matcher.ParameterComparisonResult(Parameter.compare(lhs: lhsFrom, rhs: rhsFrom, with: matcher), lhsFrom, rhsFrom, "from"))
                    return Matcher.ComparisonResult(results)

                case let (.m_presentLaunch__from_fromlaunch_launch(lhsFrom, lhsLaunch), .m_presentLaunch__from_fromlaunch_launch(rhsFrom, rhsLaunch)):
                    var results: [Matcher.ParameterComparisonResult] = []
                    results.append(Matcher.ParameterComparisonResult(Parameter.compare(lhs: lhsFrom, rhs: rhsFrom, with: matcher), lhsFrom, rhsFrom, "from"))
                    results.append(Matcher.ParameterComparisonResult(Parameter.compare(lhs: lhsLaunch, rhs: rhsLaunch, with: matcher), lhsLaunch, rhsLaunch, "launch"))
                    return Matcher.ComparisonResult(results)
                default: return .none
            }
        }

        func intValue() -> Int {
            switch self {
                case let .m_presentFilters__from_from(p0): return p0.intValue
                case let .m_presentLaunch__from_fromlaunch_launch(p0, p1): return p0.intValue + p1.intValue
            }
        }

        func assertionName() -> String {
            switch self {
                case .m_presentFilters__from_from: return ".presentFilters(from:)"
                case .m_presentLaunch__from_fromlaunch_launch: return ".presentLaunch(from:launch:)"
            }
        }
    }

    open class Given: StubbedMethod {
        fileprivate var method: MethodType

        private init(method: MethodType, products: [StubProduct]) {
            self.method = method
            super.init(products)
        }
    }

    public struct Verify {
        fileprivate var method: MethodType

        public static func presentFilters(from: Parameter<MainViewController>) -> Verify { return Verify(method: .m_presentFilters__from_from(from)) }
        public static func presentLaunch(from: Parameter<MainViewController>, launch: Parameter<Launch>) -> Verify { return Verify(method: .m_presentLaunch__from_fromlaunch_launch(from, launch)) }
    }

    public struct Perform {
        fileprivate var method: MethodType
        var performs: Any

        public static func presentFilters(from: Parameter<MainViewController>, perform: @escaping (MainViewController) -> Void) -> Perform {
            return Perform(method: .m_presentFilters__from_from(from), performs: perform)
        }

        public static func presentLaunch(from: Parameter<MainViewController>, launch: Parameter<Launch>, perform: @escaping (MainViewController, Launch) -> Void) -> Perform {
            return Perform(method: .m_presentLaunch__from_fromlaunch_launch(from, launch), performs: perform)
        }
    }

    public func given(_ method: Given) {
        methodReturnValues.append(method)
    }

    public func perform(_ method: Perform) {
        methodPerformValues.append(method)
        methodPerformValues.sort { $0.method.intValue() < $1.method.intValue() }
    }

    public func verify(_ method: Verify, count: Count = Count.moreOrEqual(to: 1), file: StaticString = #file, line: UInt = #line) {
        let fullMatches = matchingCalls(method, file: file, line: line)
        let success = count.matches(fullMatches)
        let assertionName = method.method.assertionName()
        let feedback: String = {
            guard !success else { return "" }
            return Utils.closestCallsMessage(
                for: self.invocations.map { invocation in
                    matcher.set(file: file, line: line)
                    defer { matcher.clearFileAndLine() }
                    return MethodType.compareParameters(lhs: invocation, rhs: method.method, matcher: matcher)
                },
                name: assertionName)
        }()
        MockyAssert(success, "Expected: \(count) invocations of `\(assertionName)`, but was: \(fullMatches).\(feedback)", file: file, line: line)
    }

    private func addInvocation(_ call: MethodType) {
        queue.sync { invocations.append(call) }
    }

    private func methodReturnValue(_ method: MethodType) throws -> StubProduct {
        matcher.set(file: file, line: line)
        defer { matcher.clearFileAndLine() }
        let candidates = sequencingPolicy.sorted(methodReturnValues, by: { $0.method.intValue() > $1.method.intValue() })
        let matched = candidates.first(where: { $0.isValid && MethodType.compareParameters(lhs: $0.method, rhs: method, matcher: matcher).isFullMatch })
        guard let product = matched?.getProduct(policy: stubbingPolicy) else { throw MockError.notStubed }
        return product
    }

    private func methodPerformValue(_ method: MethodType) -> Any? {
        matcher.set(file: file, line: line)
        defer { matcher.clearFileAndLine() }
        let matched = methodPerformValues.reversed().first { MethodType.compareParameters(lhs: $0.method, rhs: method, matcher: matcher).isFullMatch }
        return matched?.performs
    }

    private func matchingCalls(_ method: MethodType, file: StaticString?, line: UInt?) -> [MethodType] {
        matcher.set(file: file ?? self.file, line: line ?? self.line)
        defer { matcher.clearFileAndLine() }
        return invocations.filter { MethodType.compareParameters(lhs: $0, rhs: method, matcher: matcher).isFullMatch }
    }

    private func matchingCalls(_ method: Verify, file: StaticString?, line: UInt?) -> Int {
        return matchingCalls(method.method, file: file, line: line).count
    }

    private func givenGetterValue<T>(_ method: MethodType, _ message: String) -> T {
        do {
            return try methodReturnValue(method).casted()
        } catch {
            onFatalFailure(message)
            Failure(message)
        }
    }

    private func optionalGivenGetterValue<T>(_ method: MethodType, _: String) -> T? {
        do {
            return try methodReturnValue(method).casted()
        } catch {
            return nil
        }
    }

    private func onFatalFailure(_ message: String) {
        guard let file = self.file, let line = self.line else { return } // Let if fail if cannot handle gratefully
        SwiftyMockyTestObserver.handleFatalError(message: message, file: file, line: line)
    }
}

// MARK: - MainViewModel

open class MainViewModelMock: MainViewModel, Mock {
    public init(sequencing sequencingPolicy: SequencingPolicy = .lastWrittenResolvedFirst, stubbing stubbingPolicy: StubbingPolicy = .wrap, file: StaticString = #file, line: UInt = #line) {
        SwiftyMockyTestObserver.setup()
        self.sequencingPolicy = sequencingPolicy
        self.stubbingPolicy = stubbingPolicy
        self.file = file
        self.line = line
    }

    var matcher = Matcher.default
    var stubbingPolicy: StubbingPolicy = .wrap
    var sequencingPolicy: SequencingPolicy = .lastWrittenResolvedFirst

    private var queue = DispatchQueue(label: "com.swiftymocky.invocations", qos: .userInteractive)
    private var invocations: [MethodType] = []
    private var methodReturnValues: [Given] = []
    private var methodPerformValues: [Perform] = []
    private var file: StaticString?
    private var line: UInt?

    public typealias PropertyStub = Given
    public typealias MethodStub = Given
    public typealias SubscriptStub = Given

    /// Convenience method - call setupMock() to extend debug information when failure occurs
    public func setupMock(file: StaticString = #file, line: UInt = #line) {
        self.file = file
        self.line = line
    }

    /// Clear mock internals. You can specify what to reset (invocations aka verify, givens or performs) or leave it empty to clear all mock internals
    public func resetMock(_ scopes: MockScope...) {
        let scopes: [MockScope] = scopes.isEmpty ? [.invocation, .given, .perform] : scopes
        if scopes.contains(.invocation) { invocations = [] }
        if scopes.contains(.given) { methodReturnValues = [] }
        if scopes.contains(.perform) { methodPerformValues = [] }
    }

    public var errorHandler: AlertError? {
        get { invocations.append(.p_errorHandler_get); return __p_errorHandler ?? optionalGivenGetterValue(.p_errorHandler_get, "MainViewModelMock - stub value for errorHandler was not defined") }
        set { invocations.append(.p_errorHandler_set(.value(newValue))); __p_errorHandler = newValue }
    }

    private var __p_errorHandler: (AlertError)?

    public var companyInfo: BehaviorSubject<CompanyInfo?> {
        get { invocations.append(.p_companyInfo_get); return __p_companyInfo ?? givenGetterValue(.p_companyInfo_get, "MainViewModelMock - stub value for companyInfo was not defined") }
        set { invocations.append(.p_companyInfo_set(.value(newValue))); __p_companyInfo = newValue }
    }

    private var __p_companyInfo: (BehaviorSubject<CompanyInfo?>)?

    public var launches: BehaviorSubject<[Launch?]?> {
        get { invocations.append(.p_launches_get); return __p_launches ?? givenGetterValue(.p_launches_get, "MainViewModelMock - stub value for launches was not defined") }
        set { invocations.append(.p_launches_set(.value(newValue))); __p_launches = newValue }
    }

    private var __p_launches: (BehaviorSubject<[Launch?]?>)?

    open func refresh() {
        addInvocation(.m_refresh)
        let perform = methodPerformValue(.m_refresh) as? () -> Void
        perform?()
    }

    open func updatedFilters() {
        addInvocation(.m_updatedFilters)
        let perform = methodPerformValue(.m_updatedFilters) as? () -> Void
        perform?()
    }

    fileprivate enum MethodType {
        case m_refresh
        case m_updatedFilters
        case p_errorHandler_get
        case p_errorHandler_set(Parameter<AlertError?>)
        case p_companyInfo_get
        case p_companyInfo_set(Parameter<BehaviorSubject<CompanyInfo?>>)
        case p_launches_get
        case p_launches_set(Parameter<BehaviorSubject<[Launch?]?>>)

        static func compareParameters(lhs: MethodType, rhs: MethodType, matcher: Matcher) -> Matcher.ComparisonResult {
            switch (lhs, rhs) {
                case (.m_refresh, .m_refresh): return .match

                case (.m_updatedFilters, .m_updatedFilters): return .match
                case (.p_errorHandler_get, .p_errorHandler_get): return Matcher.ComparisonResult.match
                case let (.p_errorHandler_set(left), .p_errorHandler_set(right)): return Matcher.ComparisonResult([Matcher.ParameterComparisonResult(Parameter<AlertError?>.compare(lhs: left, rhs: right, with: matcher), left, right, "newValue")])
                case (.p_companyInfo_get, .p_companyInfo_get): return Matcher.ComparisonResult.match
                case let (.p_companyInfo_set(left), .p_companyInfo_set(right)): return Matcher.ComparisonResult([Matcher.ParameterComparisonResult(Parameter<BehaviorSubject<CompanyInfo?>>.compare(lhs: left, rhs: right, with: matcher), left, right, "newValue")])
                case (.p_launches_get, .p_launches_get): return Matcher.ComparisonResult.match
                case let (.p_launches_set(left), .p_launches_set(right)): return Matcher.ComparisonResult([Matcher.ParameterComparisonResult(Parameter<BehaviorSubject<[Launch?]?>>.compare(lhs: left, rhs: right, with: matcher), left, right, "newValue")])
                default: return .none
            }
        }

        func intValue() -> Int {
            switch self {
                case .m_refresh: return 0
                case .m_updatedFilters: return 0
                case .p_errorHandler_get: return 0
                case let .p_errorHandler_set(newValue): return newValue.intValue
                case .p_companyInfo_get: return 0
                case let .p_companyInfo_set(newValue): return newValue.intValue
                case .p_launches_get: return 0
                case let .p_launches_set(newValue): return newValue.intValue
            }
        }

        func assertionName() -> String {
            switch self {
                case .m_refresh: return ".refresh()"
                case .m_updatedFilters: return ".updatedFilters()"
                case .p_errorHandler_get: return "[get] .errorHandler"
                case .p_errorHandler_set: return "[set] .errorHandler"
                case .p_companyInfo_get: return "[get] .companyInfo"
                case .p_companyInfo_set: return "[set] .companyInfo"
                case .p_launches_get: return "[get] .launches"
                case .p_launches_set: return "[set] .launches"
            }
        }
    }

    open class Given: StubbedMethod {
        fileprivate var method: MethodType

        private init(method: MethodType, products: [StubProduct]) {
            self.method = method
            super.init(products)
        }

        public static func errorHandler(getter defaultValue: AlertError?...) -> PropertyStub {
            return Given(method: .p_errorHandler_get, products: defaultValue.map { StubProduct.return($0 as Any) })
        }

        public static func companyInfo(getter defaultValue: BehaviorSubject<CompanyInfo?>...) -> PropertyStub {
            return Given(method: .p_companyInfo_get, products: defaultValue.map { StubProduct.return($0 as Any) })
        }

        public static func launches(getter defaultValue: BehaviorSubject<[Launch?]?>...) -> PropertyStub {
            return Given(method: .p_launches_get, products: defaultValue.map { StubProduct.return($0 as Any) })
        }
    }

    public struct Verify {
        fileprivate var method: MethodType

        public static func refresh() -> Verify { return Verify(method: .m_refresh) }
        public static func updatedFilters() -> Verify { return Verify(method: .m_updatedFilters) }
        public static var errorHandler: Verify { return Verify(method: .p_errorHandler_get) }
        public static func errorHandler(set newValue: Parameter<AlertError?>) -> Verify { return Verify(method: .p_errorHandler_set(newValue)) }
        public static var companyInfo: Verify { return Verify(method: .p_companyInfo_get) }
        public static func companyInfo(set newValue: Parameter<BehaviorSubject<CompanyInfo?>>) -> Verify { return Verify(method: .p_companyInfo_set(newValue)) }
        public static var launches: Verify { return Verify(method: .p_launches_get) }
        public static func launches(set newValue: Parameter<BehaviorSubject<[Launch?]?>>) -> Verify { return Verify(method: .p_launches_set(newValue)) }
    }

    public struct Perform {
        fileprivate var method: MethodType
        var performs: Any

        public static func refresh(perform: @escaping () -> Void) -> Perform {
            return Perform(method: .m_refresh, performs: perform)
        }

        public static func updatedFilters(perform: @escaping () -> Void) -> Perform {
            return Perform(method: .m_updatedFilters, performs: perform)
        }
    }

    public func given(_ method: Given) {
        methodReturnValues.append(method)
    }

    public func perform(_ method: Perform) {
        methodPerformValues.append(method)
        methodPerformValues.sort { $0.method.intValue() < $1.method.intValue() }
    }

    public func verify(_ method: Verify, count: Count = Count.moreOrEqual(to: 1), file: StaticString = #file, line: UInt = #line) {
        let fullMatches = matchingCalls(method, file: file, line: line)
        let success = count.matches(fullMatches)
        let assertionName = method.method.assertionName()
        let feedback: String = {
            guard !success else { return "" }
            return Utils.closestCallsMessage(
                for: self.invocations.map { invocation in
                    matcher.set(file: file, line: line)
                    defer { matcher.clearFileAndLine() }
                    return MethodType.compareParameters(lhs: invocation, rhs: method.method, matcher: matcher)
                },
                name: assertionName)
        }()
        MockyAssert(success, "Expected: \(count) invocations of `\(assertionName)`, but was: \(fullMatches).\(feedback)", file: file, line: line)
    }

    private func addInvocation(_ call: MethodType) {
        queue.sync { invocations.append(call) }
    }

    private func methodReturnValue(_ method: MethodType) throws -> StubProduct {
        matcher.set(file: file, line: line)
        defer { matcher.clearFileAndLine() }
        let candidates = sequencingPolicy.sorted(methodReturnValues, by: { $0.method.intValue() > $1.method.intValue() })
        let matched = candidates.first(where: { $0.isValid && MethodType.compareParameters(lhs: $0.method, rhs: method, matcher: matcher).isFullMatch })
        guard let product = matched?.getProduct(policy: stubbingPolicy) else { throw MockError.notStubed }
        return product
    }

    private func methodPerformValue(_ method: MethodType) -> Any? {
        matcher.set(file: file, line: line)
        defer { matcher.clearFileAndLine() }
        let matched = methodPerformValues.reversed().first { MethodType.compareParameters(lhs: $0.method, rhs: method, matcher: matcher).isFullMatch }
        return matched?.performs
    }

    private func matchingCalls(_ method: MethodType, file: StaticString?, line: UInt?) -> [MethodType] {
        matcher.set(file: file ?? self.file, line: line ?? self.line)
        defer { matcher.clearFileAndLine() }
        return invocations.filter { MethodType.compareParameters(lhs: $0, rhs: method, matcher: matcher).isFullMatch }
    }

    private func matchingCalls(_ method: Verify, file: StaticString?, line: UInt?) -> Int {
        return matchingCalls(method.method, file: file, line: line).count
    }

    private func givenGetterValue<T>(_ method: MethodType, _ message: String) -> T {
        do {
            return try methodReturnValue(method).casted()
        } catch {
            onFatalFailure(message)
            Failure(message)
        }
    }

    private func optionalGivenGetterValue<T>(_ method: MethodType, _: String) -> T? {
        do {
            return try methodReturnValue(method).casted()
        } catch {
            return nil
        }
    }

    private func onFatalFailure(_ message: String) {
        guard let file = self.file, let line = self.line else { return } // Let if fail if cannot handle gratefully
        SwiftyMockyTestObserver.handleFatalError(message: message, file: file, line: line)
    }
}
