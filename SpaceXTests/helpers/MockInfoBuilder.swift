//
//  MockInfoBuilder.swift
//  MockInfoBuilder
//
//  Created by Joao Fonseca on 16/08/2021.
//

@testable import SpaceX
import Foundation

class MockInfoBuilder {
    static let shared = MockInfoBuilder()

    func getLauchInformtion() -> [Launch?] {
        let jsonDecoder = JSONDecoder()
        if let data = jsonReader(forName: "launch"), let launches = try? jsonDecoder.decode([Launch?].self, from: data) {
            return launches
        }

        return []
    }

    func getCompanyInformtion() -> CompanyInfo? {
        let jsonDecoder = JSONDecoder()
        if let data = jsonReader(forName: "info"), let companyInfo = try? jsonDecoder.decode(CompanyInfo.self, from: data) {
            return companyInfo
        }

        return nil
    }

    private func jsonReader(forName name: String) -> Data? {
        let testBundle = Bundle(for: type(of: self))
        if let path = testBundle.path(forResource: name, ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                return data
            } catch {
                return nil
            }
        }

        return nil
    }
}
