//
//  AlertError.swift
//  AlertError
//
//  Created by Joao Fonseca on 13/08/2021.
//

import Foundation

protocol AlertError: AnyObject {
    func showError(error: ApiError)
}
