//
//  DateHelper.swift
//  DateHelper
//
//  Created by Joao Fonseca on 15/08/2021.
//

import Foundation

class DateHelper {
    static func getDate(_ dateString: String) -> Date? {
        return utcDateFormatterMilliseconds.date(from: dateString)
    }

    static func getDateMissionString(_ date: Date) -> String {
        var dateString = ""
        let at = NSLocalizedString("AT", comment: "")
        var timeString = ""
        let formatter = DateFormatter()
        formatter.dateStyle = .short
        formatter.timeStyle = .none

        dateString = formatter.string(from: date)

        formatter.timeStyle = .short
        formatter.dateStyle = .none

        timeString = formatter.string(from: date)

        return "\(dateString) \(at) \(timeString)"
    }

    static func isFromPast(_ date: Date) -> Bool {
        return date <= Date()
    }

    static func getDaysSince(_ date: Date) -> Int {
        return intrevalBetweenDates(date1: date, date2: Date())
    }

    static func getDaysFrom(_ date: Date) -> Int {
        return intrevalBetweenDates(date1: Date(), date2: date)
    }

    private static func _dateFormatter(with format: String) -> DateFormatter {
        let formatter = DateFormatter()
        formatter.locale = Locale.current
        formatter.timeZone = TimeZone(abbreviation: "UTC")
        formatter.dateFormat = format
        return formatter
    }

    private static let utcDateFormatterMilliseconds: DateFormatter = {
        _dateFormatter(with: "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    }()

    private static func intrevalBetweenDates(date1: Date, date2: Date) -> Int {
        let calendar = Calendar.current
        let start = calendar.startOfDay(for: date1)
        let end = calendar.startOfDay(for: date2)

        let components = calendar.dateComponents([.day], from: start, to: end)
        return components.day ?? 0
    }
}
