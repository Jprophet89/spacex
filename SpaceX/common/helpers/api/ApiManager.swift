//
//  ApiManager.swift
//  ApiManager
//
//  Created by Joao Fonseca on 13/08/2021.
//

import Foundation

enum ApiUrl: String {
    case baseUrl
    case companyInfo
    case launches
}

class ApiManager {
    static let shared = ApiManager()
    private var urlDictionary: [String: String] = [:]

    init() {
        if let path = Bundle.main.path(forResource: "ApiList", ofType: "plist"), let info = NSDictionary(contentsOfFile: path) as? [String: String] {
            urlDictionary = info
        }
    }

    public func getUrl(apiUrl: ApiUrl, withFilter filter: Filter? = nil) -> URL? {
        var urlFilter = ""
        if let filter = filter {
            urlFilter += "?order=\(filter.order.lowercased())"
            if filter.year != NSLocalizedString("FILTER_ALL_YEARS", comment: "") {
                urlFilter += "&launch_year=\(filter.year)"
            }

            if filter.onlySuccess {
                urlFilter += "&launch_success=true"
            }
        }

        guard let apiUrl = getUrlWithBase(api: apiUrl), let url = URL(string: apiUrl + urlFilter) else {
            return nil
        }

        return url
    }

    private func getUrlWithBase(api: ApiUrl) -> String? {
        if let baseUrl = urlDictionary[ApiUrl.baseUrl.rawValue], let apiUrl = urlDictionary[api.rawValue] {
            return baseUrl + apiUrl
        }

        return nil
    }
}
