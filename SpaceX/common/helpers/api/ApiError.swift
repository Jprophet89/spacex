//
//  ApiError.swift
//  ApiError
//
//  Created by Joao Fonseca on 13/08/2021.
//

import Foundation

enum ApiError: Error {
    case badData
    case badRequest
    case unauthorized
    case forbidden
    case notFound
    case timeout
    case serverError
    case unknown

    static func fromCode(code: Int) -> ApiError {
        switch code {
            case 200:
                return .badData
            case 400:
                return .badRequest
            case 401:
                return .unauthorized
            case 403:
                return .forbidden
            case 404:
                return .notFound
            case 408:
                return .timeout
            case 500:
                return .serverError
            default:
                return .unknown
        }
    }

    static func getErrorMessage(code: Int) -> String {
        switch code {
            case 200:
                return NSLocalizedString("ALERT_BAD_DATA", comment: "")
            case 400:
                return NSLocalizedString("ALERT_BAD_REQUEST", comment: "")
            case 401:
                return NSLocalizedString("ALERT_UNAUTHORIZED", comment: "")
            case 403:
                return NSLocalizedString("ALERT_FORBIDDEN", comment: "")
            case 404:
                return NSLocalizedString("ALERT_NOT_FOUND", comment: "")
            case 408:
                return NSLocalizedString("ALERT_TIMEOUT", comment: "")
            case 500:
                return NSLocalizedString("ALERT_SERVER_ERRROR", comment: "")
            default:
                return NSLocalizedString("ALERT_UNKNOWN", comment: "")
        }
    }
}
