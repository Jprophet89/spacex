//
//  Filter.swift
//  Filter
//
//  Created by Joao Fonseca on 16/08/2021.
//

import Foundation

private let DEFAULT_YEAR = 2002

class FilterHelper {
    @Inject var companyService: CompanyService
    static let share = FilterHelper()
    static let filter_key = "FILTERS"
    static let filter_notification = NSNotification.Name("FILTERSUPDATE")
    var yearsForFilter: [String] = []

    init() {
        var firstYear = DEFAULT_YEAR
        guard let currentYear = Calendar.current.dateComponents([.year], from: Date()).year else {
            return
        }

        if let company = companyService.get() {
            firstYear = company.founded
        }

        yearsForFilter = []
        for year in firstYear ... currentYear {
            yearsForFilter.append("\(year)")
        }
        yearsForFilter.reverse()
    }

    func set(filter: Filter) {
        let jsonEncode = JSONEncoder()
        let data = try? jsonEncode.encode(filter)
        UserDefaults.standard.set(data, forKey: FilterHelper.filter_key)
    }

    func get() -> Filter {
        let jsonDecoder = JSONDecoder()
        if let data = UserDefaults.standard.value(forKey: FilterHelper.filter_key) as? Data,
           let filter = try? jsonDecoder.decode(Filter.self, from: data) {
            return filter
        }

        return Filter()
    }
}
