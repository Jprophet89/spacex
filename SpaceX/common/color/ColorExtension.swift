//
//  ColorExtension.swift
//  ColorExtension
//
//  Created by Joao Fonseca on 13/08/2021.
//

import UIKit

extension UIColor {
    // Generic
    static let background = UIColor(named: "background")
    static let text = UIColor(named: "text")

    // Table colors
    static let header = UIColor(named: "header")
    static let headerText = UIColor(named: "headerText")
}
