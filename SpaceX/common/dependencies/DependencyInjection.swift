//
//  DependencyInjection.swift
//  DependencyInjection
//
//  Created by Joao Fonseca on 13/08/2021.
//

import Foundation
import Swinject
import SwinjectAutoregistration
import UIKit

@propertyWrapper
struct Inject<Service> {
    private var service: Service

    public init() {
        service = DependencyInjection.sharedContainer().resolve(Service.self)!
    }

    var wrappedValue: Service {
        get { return service }
        mutating set { service = newValue }
    }

    var projectedValue: Inject<Service> { self }
}

class DependencyInjection: NSObject {
    private let container = Container()
    private var dependenciesLoaded: Bool = false

    static func sharedContainer() -> Container {
        return (UIApplication.shared.delegate as! AppDelegate).dependencyInjection.getContainer()
    }

    func getContainer() -> Container {
        return container
    }

    override init() {
        super.init()
        loadDependencies()
    }

    private func loadDependencies() {
        loadRepositories()
        loadServices()
        loadViewMain()
        loadViewLaunchDetail()
    }

    private func loadRepositories() {
        container.autoregister(CompanyRepository.self, initializer: DefaultCompanyRepository.init)
        container.autoregister(LaunchRepository.self, initializer: DefaultLaunchRepository.init)
    }

    private func loadServices() {
        container.autoregister(CompanyService.self, initializer: DefaultCompanyService.init)
        container.autoregister(LaunchService.self, initializer: DefaultLaunchService.init)
    }

    private func loadViewMain() {
        container.autoregister(MainViewModel.self, initializer: DefaultMainViewModel.init)
        container.autoregister(MainViewCoordinator.self, initializer: DefaultMainViewCoordinator.init)
    }

    private func loadViewLaunchDetail() {
        container.autoregister(LaunchDetailViewModel.self, initializer: DefaultLaunchDetailViewModel.init)
        container.autoregister(LaunchDetailViewCoordinator.self, initializer: DefaultLaunchDetailViewCoordinator.init)
    }
}
