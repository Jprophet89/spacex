//
//  CompanyRepository.swift
//  CompanyRepository
//
//  Created by Joao Fonseca on 13/08/2021.
//

import Foundation

// sourcery: AutoMockable
protocol CompanyRepository: AnyObject {
    func getInfo() -> CompanyInfo?
    func refresh(completion: @escaping (Result<CompanyInfo, ApiError>) -> Void)
}
