//
//  DefaultCompanyRepository.swift
//  DefaultCompanyRepository
//
//  Created by Joao Fonseca on 13/08/2021.
//

import Alamofire

class DefaultCompanyRepository: CompanyRepository {
    private let COMPANY_INFO = "COMPANY_INFO"

    func getInfo() -> CompanyInfo? {
        let jsonDecoder = JSONDecoder()

        guard let data = UserDefaults.standard.value(forKey: COMPANY_INFO) as? Data,
              let companyInfo = try? jsonDecoder.decode(CompanyInfo.self, from: data) else {
            return nil
        }

        return companyInfo
    }

    func refresh(completion: @escaping (Result<CompanyInfo, ApiError>) -> Void) {
        guard let url = ApiManager.shared.getUrl(apiUrl: .companyInfo) else {
            return completion(.failure(.unknown))
        }

        URLCache.shared.removeAllCachedResponses()
        AF.request(url, method: .get).response { [weak self] response in
            guard let self = self else {
                completion(.failure(.unknown))
                return
            }

            switch response.result {
                case let .success(data):
                    guard let data = data else {
                        completion(.failure(.badData))
                        return
                    }

                    do {
                        UserDefaults.standard.set(data, forKey: self.COMPANY_INFO)

                        let jsonDecoder = JSONDecoder()
                        let companyInfo = try jsonDecoder.decode(CompanyInfo.self, from: data)
                        completion(.success(companyInfo))
                    } catch {
                        completion(.failure(.badData))
                    }
                case let .failure(error):
                    completion(.failure(ApiError.fromCode(code: (error as NSError).code)))
            }
        }
    }
}
