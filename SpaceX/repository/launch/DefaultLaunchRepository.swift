//
//  DefaultLaunchRepository.swift
//  DefaultLaunchRepository
//
//  Created by Joao Fonseca on 15/08/2021.
//

import Alamofire
import UIKit

class DefaultLaunchRepository: LaunchRepository {
    private let LAUNCHES_INFO = "LAUNCHES_INFO"

    func get() -> [Launch?]? {
        let jsonDecoder = JSONDecoder()

        guard let data = UserDefaults.standard.value(forKey: LAUNCHES_INFO) as? Data,
              let launches = try? jsonDecoder.decode([Launch?].self, from: data) else {
            return nil
        }

        return launches
    }

    func refresh(filter: Filter?, completion: @escaping (Result<[Launch?], ApiError>) -> Void) {
        guard let url = ApiManager.shared.getUrl(apiUrl: .launches, withFilter: filter) else {
            return completion(.failure(.unknown))
        }

        URLCache.shared.removeAllCachedResponses()
        AF.request(url, method: .get).response { [weak self] response in
            guard let self = self else {
                completion(.failure(.unknown))
                return
            }

            switch response.result {
                case let .success(data):
                    guard let data = data else {
                        completion(.failure(.badData))
                        return
                    }

                    do {
                        UserDefaults.standard.set(data, forKey: self.LAUNCHES_INFO)

                        let jsonDecoder = JSONDecoder()
                        let launches = try jsonDecoder.decode([Launch?].self, from: data)
                        completion(.success(launches))
                    } catch {
                        completion(.failure(.badData))
                    }
                case let .failure(error):
                    completion(.failure(ApiError.fromCode(code: (error as NSError).code)))
            }
        }
    }
}
