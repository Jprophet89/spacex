//
//  LaunchRepository.swift
//  LaunchRepository
//
//  Created by Joao Fonseca on 15/08/2021.
//

import UIKit

// sourcery: AutoMockable
protocol LaunchRepository: AnyObject {
    func get() -> [Launch?]?
    func refresh(filter: Filter?, completion: @escaping (Result<[Launch?], ApiError>) -> Void)
}
