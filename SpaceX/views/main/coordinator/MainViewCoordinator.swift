//
//  MainViewCoordinator.swift
//  MainViewCoordinator
//
//  Created by Joao Fonseca on 13/08/2021.
//

import UIKit

// sourcery: AutoMockable
protocol MainViewCoordinator {
    func presentFilters(from: MainViewController)
    func presentLaunch(from: MainViewController, launch: Launch)
}
