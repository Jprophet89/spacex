//
//  DefaultMainViewCoordinator.swift
//  DefaultMainViewCoordinator
//
//  Created by Joao Fonseca on 13/08/2021.
//

import UIKit

class DefaultMainViewCoordinator: MainViewCoordinator {
    func presentFilters(from vc: MainViewController) {
        let filterPopup = FiltersPopupViewController()
        filterPopup.delegate = vc
        let popupSlide = PopUpSlideManager()
        popupSlide.direction = .top
        filterPopup.transitioningDelegate = popupSlide
        filterPopup.modalPresentationStyle = .custom
        vc.present(filterPopup, animated: true, completion: nil)
    }

    func presentLaunch(from vc: MainViewController, launch: Launch) {
        let launchDetailVC = LaunchDetailViewController()
        launchDetailVC.setLaunch(launch: launch)
        launchDetailVC.modalPresentationStyle = .pageSheet
        vc.present(launchDetailVC, animated: true, completion: nil)
    }
}
