//
//  MainViewController.swift
//  SpaceX
//
//  Created by Joao Fonseca on 13/08/2021.
//

import RxSwift
import UIKit

class MainViewController: UIViewController {
    let mainView = MainView()
    @Inject var viewModel: MainViewModel
    @Inject var viewCoordinator: MainViewCoordinator
    private var disposeBag = DisposeBag()
    var hasAlert = false

    override func viewDidLoad() {
        super.viewDidLoad()
        title = NSLocalizedString("MAIN_TITLE", comment: "")
        setNavigationBar()
        loadMainView()
        loadViewModel()
    }

    private func setNavigationBar() {
        let container = UIView()
        container.accessibilityIdentifier = "filterButton"
        container.accessibilityTraits = .button
        let imageView = UIImageView()
        imageView.image = UIImage(named: "navFilter")
        [container, imageView].forEach {
            $0.translatesAutoresizingMaskIntoConstraints = false
        }

        container.addSubview(imageView)

        NSLayoutConstraint.activate([
            container.widthAnchor.constraint(equalToConstant: 20),
            container.heightAnchor.constraint(equalToConstant: 20),
            imageView.topAnchor.constraint(equalTo: container.topAnchor),
            imageView.leftAnchor.constraint(equalTo: container.leftAnchor),
            imageView.bottomAnchor.constraint(equalTo: container.bottomAnchor),
            imageView.rightAnchor.constraint(equalTo: container.rightAnchor),
        ])

        container.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openFilters)))
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: container)
    }

    private func loadMainView() {
        mainView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(mainView)
        mainView.load()
        mainView.delegate = self

        NSLayoutConstraint.activate([
            mainView.topAnchor.constraint(equalTo: view.topAnchor),
            mainView.leftAnchor.constraint(equalTo: view.leftAnchor),
            mainView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            mainView.rightAnchor.constraint(equalTo: view.rightAnchor),
        ])

        viewModel.companyInfo.asObserver().subscribe(onNext: { [weak self] companyInfo in
            self?.mainView.stopRefresh()
            guard let companyInfo = companyInfo else {
                return
            }

            self?.mainView.updateCompanyInfo(companyInfo)
        }, onError: nil, onCompleted: nil, onDisposed: nil).disposed(by: disposeBag)

        viewModel.launches.asObserver().subscribe(onNext: { [weak self] launches in
            self?.mainView.stopRefresh()
            guard let launches = launches else {
                return
            }

            self?.mainView.updateLaunches(launches)
        }, onError: nil, onCompleted: nil, onDisposed: nil).disposed(by: disposeBag)
    }

    private func loadViewModel() {
        viewModel.errorHandler = self
        viewModel.refresh()
    }

    @objc func openFilters() {
        viewCoordinator.presentFilters(from: self)
    }
}

extension MainViewController: FilterProtocol {
    func updateFilter() {
        viewModel.updatedFilters()
    }
}

extension MainViewController: MainViewActions {
    func selectedLaunch(launch: Launch) {
        viewCoordinator.presentLaunch(from: self, launch: launch)
    }

    func refreshInformation() {
        viewModel.refresh()
    }
}

extension MainViewController: AlertError {
    func showError(error: ApiError) {
        if !hasAlert {
            hasAlert = true
            let alert = UIAlertController(title: NSLocalizedString("ALERT_TITLE", comment: ""), message: ApiError.getErrorMessage(code: (error as NSError).code), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("ALERT_DISMISS", comment: ""), style: .default, handler: { [weak self] _ in
                self?.hasAlert = false
                alert.dismiss(animated: true, completion: nil)
            }))
            present(alert, animated: true, completion: nil)
        }
    }
}
