//
//  MainViewModel.swift
//  MainViewModel
//
//  Created by Joao Fonseca on 13/08/2021.
//
import RxSwift

// sourcery: AutoMockable
protocol MainViewModel {
    var errorHandler: AlertError? { get set }
    var companyInfo: BehaviorSubject<CompanyInfo?> { get set }
    var launches: BehaviorSubject<[Launch?]?> { get set }
    func refresh()
    func updatedFilters()
}
