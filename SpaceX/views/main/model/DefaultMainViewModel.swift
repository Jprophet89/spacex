//
//  DefaultMainViewModel.swift
//  DefaultMainViewModel
//
//  Created by Joao Fonseca on 13/08/2021.
//

import Foundation
import RxSwift

class DefaultMainViewModel: MainViewModel {
    @Inject var companyService: CompanyService
    @Inject var launchesService: LaunchService

    var errorHandler: AlertError?
    var companyInfo = BehaviorSubject<CompanyInfo?>(value: nil)
    var launches = BehaviorSubject<[Launch?]?>(value: nil)
    private var isUpdating = false

    init() {
        if let companyInfo = companyService.get() {
            self.companyInfo.onNext(companyInfo)
        } else {
            refreshCompanyInfo()
        }

        if let launches = launchesService.get() {
            self.launches.onNext(launches)
        } else {
            refreshLaunches()
        }
    }

    func refresh() {
        refreshCompanyInfo()
        refreshLaunches()
    }

    func updatedFilters() {
        refreshLaunches()
    }

    private func refreshCompanyInfo() {
        companyService.refresh { [weak self] result in
            switch result {
                case let .success(companyInfo):
                    self?.companyInfo.onNext(companyInfo)
                case let .failure(error):
                    self?.companyInfo.onNext(nil)
                    self?.errorHandler?.showError(error: error)
            }
        }
    }

    private func refreshLaunches() {
        let filter = FilterHelper.share.get()
        launchesService.refresh(filter: filter, completion: { [weak self] result in
            switch result {
                case let .success(launches):
                    self?.launches.onNext(launches)
                case let .failure(error):
                    self?.launches.onNext(nil)
                    self?.errorHandler?.showError(error: error)
            }
        })
    }
}
