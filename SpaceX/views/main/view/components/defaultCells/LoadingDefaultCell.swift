//
//  LoadingDefaultCell.swift
//  SpaceX
//
//  Created by Joao Fonseca on 18/08/2021.
//

import UIKit

class LoadingDefaultCell: AbstractMessageCell {
    static let reuseIdentifier = "LoadingDefaultCell"

    override func initialize() {
        super.initialize()
        information.text = NSLocalizedString("LOADING", comment: "")
        noInfoImage.image = UIImage(named: "loading")

        let rotation = CABasicAnimation(keyPath: "transform.rotation.z")
        rotation.toValue = NSNumber(value: Double.pi * 2)
        rotation.duration = 1
        rotation.isCumulative = true
        rotation.repeatCount = Float.greatestFiniteMagnitude

        noInfoImage.layer.add(rotation, forKey: "rotation")
        noInfoImage.startAnimating()
    }
}
