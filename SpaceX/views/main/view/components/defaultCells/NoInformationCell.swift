//
//  NoConnectionCell.swift
//  NoInformationCell
//
//  Created by Joao Fonseca on 15/08/2021.
//

import UIKit

class NoInformationCell: AbstractMessageCell {
    static let reuseIdentifier = "NoInformationCell"

    override func initialize() {
        super.initialize()
        information.text = NSLocalizedString("NO_RESULTS", comment: "")
        noInfoImage.image = UIImage(named: "no_result")
    }
}
