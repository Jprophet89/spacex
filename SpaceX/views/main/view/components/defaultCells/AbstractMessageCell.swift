//
//  AbstractMessageCell.swift
//  AbstractMessageCell
//
//  Created by Joao Fonseca on 16/08/2021.
//
import UIKit

class AbstractMessageCell: UITableViewCell {
    var noInfoImage = UIImageView()
    var information = UILabel()

    func initialize() {
        [noInfoImage, information].forEach {
            $0.translatesAutoresizingMaskIntoConstraints = false
            addSubview($0)
        }

        information.numberOfLines = 0
        information.text = ""
        information.textAlignment = .center

        noInfoImage.image = nil

        NSLayoutConstraint.activate([
            noInfoImage.topAnchor.constraint(equalTo: topAnchor, constant: 16),
            noInfoImage.centerXAnchor.constraint(equalTo: centerXAnchor),
            noInfoImage.widthAnchor.constraint(equalToConstant: 40),
            noInfoImage.heightAnchor.constraint(equalToConstant: 40),

            information.topAnchor.constraint(equalTo: noInfoImage.bottomAnchor, constant: 8),
            information.leftAnchor.constraint(equalTo: leftAnchor, constant: 16),
            bottomAnchor.constraint(equalTo: information.bottomAnchor, constant: 16),
            rightAnchor.constraint(equalTo: information.rightAnchor, constant: 16),
        ])
    }
}
