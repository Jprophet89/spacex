//
//  LaunchInfoCell.swift
//  LaunchInfoCell
//
//  Created by Joao Fonseca on 15/08/2021.
//

import SDWebImage
import UIKit

class LaunchInfoCell: UITableViewCell {
    @Inject var launchService: LaunchService
    static let reuseIdentifier = "LaunchInfoCell"
    var launch: Launch?
    var launcheImage = UIImageView()
    var statusImage = UIImageView()
    var stack = UIStackView()
    var separator = UIView()

    private var missionInfo = InfoStack(frame: .zero)
    private var dateTimeInfo = InfoStack(frame: .zero)
    private var rocketInfo = InfoStack(frame: .zero)
    private var daysInfo = InfoStack(frame: .zero)

    override func prepareForReuse() {
        launcheImage.image = nil
        statusImage.image = nil
    }

    func initialize(_ launch: Launch) {
        self.launch = launch
        [launcheImage, stack, statusImage, separator].forEach {
            $0.translatesAutoresizingMaskIntoConstraints = false
            addSubview($0)
        }

        setStackInformation()
        setMissionImage()
        setStatusImage()

        stack.distribution = .fill
        stack.axis = .vertical

        separator.backgroundColor = .header

        NSLayoutConstraint.activate([
            launcheImage.topAnchor.constraint(equalTo: topAnchor, constant: 8),
            launcheImage.leftAnchor.constraint(equalTo: leftAnchor, constant: 8),
            launcheImage.widthAnchor.constraint(equalToConstant: 32),
            launcheImage.heightAnchor.constraint(equalToConstant: 32),
            separator.bottomAnchor.constraint(equalTo: bottomAnchor),
            separator.heightAnchor.constraint(equalToConstant: 1),
            separator.leftAnchor.constraint(equalTo: leftAnchor),
            separator.rightAnchor.constraint(equalTo: rightAnchor),
            stack.topAnchor.constraint(equalTo: launcheImage.topAnchor),
            stack.leftAnchor.constraint(equalTo: launcheImage.rightAnchor, constant: 0),
            separator.topAnchor.constraint(equalTo: stack.bottomAnchor, constant: 8),
            rightAnchor.constraint(equalTo: statusImage.rightAnchor, constant: 8),
            statusImage.centerYAnchor.constraint(equalTo: centerYAnchor),
            statusImage.leftAnchor.constraint(equalTo: stack.rightAnchor, constant: 4),
            statusImage.widthAnchor.constraint(equalToConstant: 32),
            statusImage.heightAnchor.constraint(equalToConstant: 32),
        ])
    }

    private func setStackInformation() {
        // add info to the stack the first time
        if stack.arrangedSubviews.isEmpty {
            stack.addArrangedSubview(missionInfo)
            stack.addArrangedSubview(dateTimeInfo)
            stack.addArrangedSubview(rocketInfo)
            stack.addArrangedSubview(daysInfo)
        }

        // fill the information
        missionInfo.updateInfo(title: NSLocalizedString("MISSION_TITLE", comment: ""), info: launch?.mission_name ?? "")
        if let date = launchService.getDate(launch: launch), let upcoming = launch?.upcoming {
            dateTimeInfo.updateInfo(title: NSLocalizedString("DATE_TIME_TITLE", comment: ""), info: DateHelper.getDateMissionString(date))

            let daysUntilNow = NSLocalizedString("DAYS_NOW_TITLE", comment: "")
            if DateHelper.isFromPast(date) {
                if upcoming {
                    // if the launche was in the past and still is upcoming is with delay so that's why i added this new label.
                    daysInfo.updateInfo(title: NSLocalizedString("DAYS_WITH_DELAY", comment: ""), info: "\(DateHelper.getDaysSince(date))")
                } else {
                    let daysSince = String(format: daysUntilNow, NSLocalizedString("DAYS_SINCE", comment: ""))
                    daysInfo.updateInfo(title: daysSince, info: "\(DateHelper.getDaysSince(date))")
                }
            } else {
                let daysFrom = String(format: daysUntilNow, NSLocalizedString("DAYS_FROM", comment: ""))
                daysInfo.updateInfo(title: daysFrom, info: "\(DateHelper.getDaysFrom(date))")
            }
        } else {
            dateTimeInfo.updateInfo(title: NSLocalizedString("DATE_TIME_TITLE", comment: ""), info: NSLocalizedString("NOT_AVAILABLE", comment: ""))
        }

        rocketInfo.updateInfo(
            title: NSLocalizedString("ROCKET_TITLE", comment: ""),
            info: launchService.getRocketInfo(launch: launch))

        stack.sizeToFit()
        stack.layoutIfNeeded()
    }

    private func setMissionImage() {
        launchService.getLaunchImage(launch: launch) { [weak self] result in
            switch result {
                case let .success(image):
                    self?.launcheImage.image = image
                case .failure:
                    self?.launcheImage.image = UIImage(named: "rocket")
            }
        }
    }

    private func setStatusImage() {
        guard let launcheStatus = launch?.launch_success else {
            // check if launche was delay
            if let dateString = launch?.launch_date_utc, let date = DateHelper.getDate(dateString), let upcoming = launch?.upcoming {
                if DateHelper.isFromPast(date) {
                    if upcoming {
                        statusImage.image = UIImage(named: "launcheDelay")
                        return
                    } else {
                        // if launche is not upcoming and in the past must be canceled
                        statusImage.image = UIImage(named: "launcheFailure")
                        return
                    }
                }
            }

            // if is in the future show calendar to add event on the calendar
            statusImage.image = UIImage(named: "launcheCalendar")
            return
        }

        if launcheStatus {
            statusImage.image = UIImage(named: "launcheSuccess")
        } else {
            statusImage.image = UIImage(named: "launcheFailure")
        }
    }
}

class InfoStack: UIView {
    private var title = UILabel()
    private var info = UILabel()

    override init(frame: CGRect) {
        super.init(frame: frame)
        [title, info].forEach {
            $0.translatesAutoresizingMaskIntoConstraints = false
            $0.numberOfLines = 0
            addSubview($0)
        }

        title.font = UIFont(name: title.font.fontName, size: 12.0)
        info.font = UIFont(name: info.font.fontName, size: 12.0)

        NSLayoutConstraint.activate([
            title.topAnchor.constraint(equalTo: topAnchor, constant: 2),
            title.leftAnchor.constraint(equalTo: leftAnchor, constant: 2),
            title.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.4),
            bottomAnchor.constraint(greaterThanOrEqualTo: title.bottomAnchor, constant: 2),
            info.topAnchor.constraint(equalTo: title.topAnchor),
            bottomAnchor.constraint(greaterThanOrEqualTo: info.bottomAnchor, constant: 2),
            rightAnchor.constraint(equalTo: info.rightAnchor, constant: 2),
            info.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.6),
        ])
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    public func updateInfo(title: String? = nil, info: String) {
        if let title = title {
            self.title.text = title
        }

        self.info.text = info
    }
}
