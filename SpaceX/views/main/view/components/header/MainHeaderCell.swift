//
//  MainHeaderCell.swift
//  MainHeaderCell
//
//  Created by Joao Fonseca on 13/08/2021.
//

import UIKit

class MainHeaderCell: UITableViewHeaderFooterView {
    static let reuseIdentifier = "MainHeaderCell"
    private let title = UILabel()

    func initialize() {
        let backView = UIView()
        backView.backgroundColor = .header
        backgroundView = backView
        title.textColor = .headerText
        title.translatesAutoresizingMaskIntoConstraints = false
        addSubview(title)
        title.numberOfLines = 0
        title.text = ""
        title.textAlignment = .natural

        NSLayoutConstraint.activate([
            title.topAnchor.constraint(equalTo: topAnchor, constant: 8),
            title.leftAnchor.constraint(equalTo: leftAnchor, constant: 16),
            bottomAnchor.constraint(equalTo: title.bottomAnchor, constant: 8),
            rightAnchor.constraint(equalTo: title.rightAnchor, constant: 16),
        ])
    }

    func setTitle(_ text: String) {
        title.text = text
    }
}
