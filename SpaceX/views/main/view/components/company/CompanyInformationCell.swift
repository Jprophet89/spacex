//
//  CompanyInformationCell.swift
//  CompanyInformationCell
//
//  Created by Joao Fonseca on 13/08/2021.
//

import UIKit

class CompanyInformationCell: UITableViewCell {
    static let reuseIdentifier = "CompanyInformationCell"
    var information = UILabel()

    func initialize() {
        accessibilityIdentifier = "companyInformationCell"
        information.translatesAutoresizingMaskIntoConstraints = false
        addSubview(information)
        information.numberOfLines = 0
        information.text = ""
        information.textAlignment = .natural

        NSLayoutConstraint.activate([
            information.topAnchor.constraint(equalTo: topAnchor, constant: 16),
            information.leftAnchor.constraint(equalTo: leftAnchor, constant: 16),
            bottomAnchor.constraint(equalTo: information.bottomAnchor, constant: 16),
            rightAnchor.constraint(equalTo: information.rightAnchor, constant: 16),
        ])
    }

    func updateCompanyInfo(_ companyInfo: CompanyInfo) {
        information.text = String(
            format: NSLocalizedString("COMPANY_DESCRIPTION", comment: ""),
            companyInfo.name,
            companyInfo.founder,
            companyInfo.founded,
            companyInfo.employees,
            companyInfo.launch_sites,
            companyInfo.valuation)
    }
}
