//
//  MainView.swift
//  MainView
//
//  Created by Joao Fonseca on 13/08/2021.
//

import UIKit

protocol MainViewActions: AnyObject {
    func refreshInformation()
    func selectedLaunch(launch: Launch)
}

final class IntrinsicTableView: UITableView {
    override var contentSize: CGSize {
        didSet {
            invalidateIntrinsicContentSize()
        }
    }

    override var intrinsicContentSize: CGSize {
        layoutIfNeeded()
        return CGSize(width: UIView.noIntrinsicMetric, height: contentSize.height)
    }
}

class MainView: UIView {
    private let refreshControl = UIRefreshControl()
    var tableView = IntrinsicTableView()
    var companyInfo: CompanyInfo?
    var launches: [Launch?]?
    weak var delegate: MainViewActions?
    var hasUpdatedLaunches = false
    var hasUpdatedCompany = false

    func load() {
        backgroundColor = .background
        setupTable()
    }

    func updateCompanyInfo(_ companyInfo: CompanyInfo) {
        self.companyInfo = companyInfo
        hasUpdatedCompany = true

        tableView.reloadData()
    }

    func updateLaunches(_ launches: [Launch?]) {
        self.launches = launches
        hasUpdatedLaunches = true

        tableView.reloadData()
    }

    func stopRefresh() {
        refreshControl.endRefreshing()
    }

    @objc private func refreshInformation() {
        delegate?.refreshInformation()
    }
}

extension MainView: UITableViewDelegate, UITableViewDataSource {
    func setupTable() {
        tableView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(tableView)
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: topAnchor),
            tableView.leftAnchor.constraint(equalTo: leftAnchor),
            bottomAnchor.constraint(equalTo: tableView.bottomAnchor),
            rightAnchor.constraint(equalTo: tableView.rightAnchor),
        ])

        tableView.accessibilityIdentifier = "mainTable"
        tableView.addSubview(refreshControl)
        refreshControl.addTarget(self, action: #selector(refreshInformation), for: .valueChanged)
        refreshControl.tintColor = .header
        refreshControl.attributedTitle = NSAttributedString(string: NSLocalizedString("FETCHING_INFORMATION", comment: ""))

        tableView.dataSource = self
        tableView.delegate = self
        tableView.separatorStyle = .none

        tableView.register(MainHeaderCell.self, forHeaderFooterViewReuseIdentifier: MainHeaderCell.reuseIdentifier)
        tableView.register(CompanyInformationCell.self, forCellReuseIdentifier: CompanyInformationCell.reuseIdentifier)
        tableView.register(LaunchInfoCell.self, forCellReuseIdentifier: LaunchInfoCell.reuseIdentifier)
        tableView.register(LoadingDefaultCell.self, forCellReuseIdentifier: LoadingDefaultCell.reuseIdentifier)
        tableView.register(NoInformationCell.self, forCellReuseIdentifier: NoInformationCell.reuseIdentifier)
    }

    func numberOfSections(in _: UITableView) -> Int {
        return 2
    }

    func tableView(_: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
            case 0:
                return 1
            case 1:
                guard let launches = launches, !launches.isEmpty else {
                    return 1
                }
                return launches.count
            default:
                return 0
        }
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        switch section {
            case 0:
                guard let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: MainHeaderCell.reuseIdentifier) as? MainHeaderCell else {
                    return nil
                }

                header.accessibilityIdentifier = "companyHeader"
                header.initialize()
                header.setTitle(NSLocalizedString("COMPANY_HEADER_CELL_TITLE", comment: ""))
                return header
            case 1:
                guard let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: MainHeaderCell.reuseIdentifier) as? MainHeaderCell else {
                    return nil
                }

                header.accessibilityIdentifier = "launchesHeader"
                header.initialize()
                header.setTitle(NSLocalizedString("LAUNCHES_HEADER_CELL_TITLE", comment: ""))
                return header
            default:
                return nil
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
            case 0: // Company Information
                guard let cell = tableView.dequeueReusableCell(withIdentifier: CompanyInformationCell.reuseIdentifier) as? CompanyInformationCell, let companyInfo = companyInfo else {
                    if let cell = tableView.dequeueReusableCell(withIdentifier: LoadingDefaultCell.reuseIdentifier) as? LoadingDefaultCell {
                        cell.initialize()
                        return cell
                    }

                    return UITableViewCell()
                }

                cell.initialize()
                cell.updateCompanyInfo(companyInfo)
                return cell
            case 1: // Launch
                // check if there is no information due to error
                guard let launches = launches else {
                    if let cell = tableView.dequeueReusableCell(withIdentifier: LoadingDefaultCell.reuseIdentifier) as? LoadingDefaultCell {
                        cell.initialize()
                        return cell
                    }

                    return UITableViewCell()
                }
                // Everything is ok, just no results return
                if launches.isEmpty {
                    if let cell = tableView.dequeueReusableCell(withIdentifier: NoInformationCell.reuseIdentifier) as? NoInformationCell {
                        cell.initialize()
                        return cell
                    }
                }
                // show LaunchCell
                if let cell = tableView.dequeueReusableCell(withIdentifier: LaunchInfoCell.reuseIdentifier) as? LaunchInfoCell,
                   let launche = launches[indexPath.row] {
                    cell.accessibilityIdentifier = "launchCell\(indexPath.row)"
                    cell.initialize(launche)

                    return cell
                }

                return UITableViewCell()
            default:
                return UITableViewCell()
        }
    }

    func tableView(_: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1, let launches = launches, let launche = launches[indexPath.row] {
            delegate?.selectedLaunch(launch: launche)
        }
    }
}
