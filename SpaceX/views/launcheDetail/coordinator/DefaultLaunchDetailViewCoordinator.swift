//
//  DefaultLaunchDetailViewCoordinator.swift
//  DefaultLaunchDetailViewCoordinator
//
//  Created by Joao Fonseca on 16/08/2021.
//

import UIKit

class DefaultLaunchDetailViewCoordinator: LaunchDetailViewCoordinator {
    func openExternalLink(url: URL) {
        UIApplication.shared.open(url)
    }
}
