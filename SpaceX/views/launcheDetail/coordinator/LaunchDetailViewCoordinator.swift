//
//  LaunchDetailViewCoordinator.swift
//  LaunchDetailViewCoordinator
//
//  Created by Joao Fonseca on 16/08/2021.
//

import Foundation

// sourcery: AutoMockable
protocol LaunchDetailViewCoordinator: AnyObject {
    func openExternalLink(url: URL)
}
