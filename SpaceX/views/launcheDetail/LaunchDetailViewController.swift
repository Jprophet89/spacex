//
//  LaunchDetailViewController.swift
//  LaunchDetailViewController
//
//  Created by Joao Fonseca on 16/08/2021.
//

import Foundation
import UIKit

class LaunchDetailViewController: UIViewController {
    @Inject var viewModel: LaunchDetailViewModel
    @Inject var viewCoordinator: LaunchDetailViewCoordinator

    override func viewDidLoad() {
        super.viewDidLoad()
        setLaunchDetailView()
    }

    private func setLaunchDetailView() {
        let launchDetailView = LaunchDetailView()
        launchDetailView.setLaunchToDisplay(viewModel.launch)
        launchDetailView.delegate = self
        launchDetailView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(launchDetailView)

        NSLayoutConstraint.activate([
            launchDetailView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            launchDetailView.leftAnchor.constraint(equalTo: view.leftAnchor),
            launchDetailView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            launchDetailView.rightAnchor.constraint(equalTo: view.rightAnchor),
        ])
    }

    func setLaunch(launch: Launch) {
        viewModel.initialize(launch)
    }
}

extension LaunchDetailViewController: LaunchDetailAction {
    func openLink(url: URL) {
        viewCoordinator.openExternalLink(url: url)
    }
}
