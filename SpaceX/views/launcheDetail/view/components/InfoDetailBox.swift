//
//  InfoDetailBox.swift
//  InfoDetailBox
//
//  Created by Joao Fonseca on 16/08/2021.
//

import UIKit

enum InfoDetailFormat {
    case article
    case wiki
    case video
}

protocol InfoDetailAction: AnyObject {
    func selected(option: InfoDetailFormat)
}

class InfoDetailBox: UIView {
    private let imageDetail = UIImageView()
    private let titleDetail = UILabel()
    var option: InfoDetailFormat = .article
    var delegate: InfoDetailAction?

    override init(frame: CGRect) {
        super.init(frame: frame)
        [imageDetail, titleDetail].forEach {
            $0.translatesAutoresizingMaskIntoConstraints = false
            addSubview($0)
        }

        NSLayoutConstraint.activate([
            imageDetail.topAnchor.constraint(equalTo: topAnchor, constant: 8),
            imageDetail.heightAnchor.constraint(equalToConstant: 42),
            imageDetail.widthAnchor.constraint(equalToConstant: 42),
            imageDetail.centerXAnchor.constraint(equalTo: centerXAnchor),
            titleDetail.topAnchor.constraint(equalTo: imageDetail.bottomAnchor, constant: 8),
            titleDetail.centerXAnchor.constraint(equalTo: centerXAnchor),
            bottomAnchor.constraint(equalTo: titleDetail.bottomAnchor, constant: 8),
        ])

        addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(selected)))
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    @objc func selected() {
        delegate?.selected(option: option)
    }

    public func setLink(option: InfoDetailFormat) {
        switch option {
            case .article:
                imageDetail.image = UIImage(named: "article")
                titleDetail.text = NSLocalizedString("ARTICLE", comment: "")
            case .wiki:
                imageDetail.image = UIImage(named: "wikipedia")
                titleDetail.text = NSLocalizedString("WIKIPEDIA", comment: "")
            case .video:
                imageDetail.image = UIImage(named: "youtube")
                titleDetail.text = NSLocalizedString("VIDEO", comment: "")
        }

        self.option = option
    }
}
