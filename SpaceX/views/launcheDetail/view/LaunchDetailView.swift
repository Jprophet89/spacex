//
//  LaunchDetailView.swift
//  LaunchDetailView
//
//  Created by Joao Fonseca on 16/08/2021.
//

import UIKit

protocol LaunchDetailAction: AnyObject {
    func openLink(url: URL)
}

class LaunchDetailView: UIView {
    @Inject var launchService: LaunchService
    // components
    let missionImage = UIImageView()
    let missionName = UILabel()
    let informationDetails = UILabel()
    let stackOptions = UIStackView()
    // properties
    weak var delegate: LaunchDetailAction?
    var launch: Launch?

    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .background

        [missionImage, missionName, informationDetails, stackOptions].forEach {
            $0.translatesAutoresizingMaskIntoConstraints = false
            addSubview($0)
        }

        missionImage.accessibilityIdentifier = "missionImage"
        missionName.accessibilityIdentifier = "missionName"
        informationDetails.accessibilityIdentifier = "informationDetails"

        missionName.numberOfLines = 0
        informationDetails.numberOfLines = 0

        stackOptions.distribution = .fill
        stackOptions.axis = .vertical
        stackOptions.spacing = 32

        NSLayoutConstraint.activate([
            // image
            missionImage.topAnchor.constraint(equalTo: topAnchor, constant: 32),
            missionImage.centerXAnchor.constraint(equalTo: centerXAnchor),
            missionImage.heightAnchor.constraint(equalToConstant: 64),
            missionImage.widthAnchor.constraint(equalToConstant: 64),
            // title
            missionName.topAnchor.constraint(equalTo: missionImage.bottomAnchor, constant: 16),
            missionName.leftAnchor.constraint(equalTo: leftAnchor, constant: 16),
            rightAnchor.constraint(equalTo: missionName.rightAnchor, constant: 16),
            // description
            informationDetails.topAnchor.constraint(equalTo: missionName.bottomAnchor, constant: 16),
            informationDetails.leftAnchor.constraint(equalTo: leftAnchor, constant: 16),
            rightAnchor.constraint(equalTo: informationDetails.rightAnchor, constant: 16),
            // stack with options
            stackOptions.topAnchor.constraint(equalTo: informationDetails.bottomAnchor, constant: 16),
            stackOptions.leftAnchor.constraint(equalTo: leftAnchor, constant: 16),
            rightAnchor.constraint(equalTo: stackOptions.rightAnchor, constant: 16),
        ])
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setLaunchToDisplay(_ launch: Launch?) {
        self.launch = launch
        refreshUI()
    }

    private func refreshUI() {
        setLaunchImage()
        missionName.text = NSLocalizedString("MISSION_NAME", comment: "") + (launch?.mission_name ?? NSLocalizedString("NO_NAME_AVAILABLE", comment: ""))
        informationDetails.text = launch?.details ?? NSLocalizedString("NO_DETIALS_AVAILABLE", comment: "")
        setActionsAvailable()
    }

    private func setLaunchImage() {
        launchService.getLaunchImage(launch: launch) { [weak self] result in
            switch result {
                case let .success(image):
                    self?.missionImage.image = image
                case .failure:
                    self?.missionImage.image = UIImage(named: "rocket")
            }
        }
    }

    private func setActionsAvailable() {
        if let _ = launch?.links.article_link {
            let article = InfoDetailBox(frame: .zero)
            article.setLink(option: .article)
            article.delegate = self
            stackOptions.addArrangedSubview(article)
        }

        if let _ = launch?.links.wikipedia {
            let wiki = InfoDetailBox(frame: .zero)
            wiki.setLink(option: .wiki)
            wiki.delegate = self
            stackOptions.addArrangedSubview(wiki)
        }

        if let _ = launch?.links.video_link {
            let video = InfoDetailBox(frame: .zero)
            video.setLink(option: .video)
            video.delegate = self
            stackOptions.addArrangedSubview(video)
        }

        stackOptions.sizeToFit()
        stackOptions.layoutIfNeeded()
    }
}

extension LaunchDetailView: InfoDetailAction {
    func selected(option: InfoDetailFormat) {
        switch option {
            case .article:
                if let link = launch?.links.article_link, let url = URL(string: link) {
                    delegate?.openLink(url: url)
                }
            case .wiki:
                if let link = launch?.links.wikipedia, let url = URL(string: link) {
                    delegate?.openLink(url: url)
                }
            case .video:
                if let link = launch?.links.video_link, let url = URL(string: link) {
                    delegate?.openLink(url: url)
                }
        }
    }
}
