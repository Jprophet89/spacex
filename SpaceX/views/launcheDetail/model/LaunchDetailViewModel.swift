//
//  LaunchDetailViewModel.swift
//  LaunchDetailViewModel
//
//  Created by Joao Fonseca on 16/08/2021.
//

import Foundation

// sourcery: AutoMockable
protocol LaunchDetailViewModel: AnyObject {
    var launch: Launch? { get set }
    func initialize(_ launch: Launch)
}
