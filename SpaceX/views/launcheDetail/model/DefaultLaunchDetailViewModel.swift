//
//  DefaultLaunchDetailViewModel.swift
//  DefaultLaunchDetailViewModel
//
//  Created by Joao Fonseca on 16/08/2021.
//

import Foundation

class DefaultLaunchDetailViewModel: LaunchDetailViewModel {
    var launch: Launch?

    func initialize(_ launch: Launch) {
        self.launch = launch
    }
}
