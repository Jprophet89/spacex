//
//  FiltersPopupViewController.swift
//  FiltersPopupViewController
//
//  Created by Joao Fonseca on 16/08/2021.
//

import UIKit

protocol FilterProtocol: AnyObject {
    func updateFilter()
}

class FiltersPopupViewController: UIViewController {
    @Inject var launcheService: LaunchService
    private let dimmingView = UIView()
    private let container = UIView()
    // filter components
    let orderPicker = UIPickerView()
    let orderPickerData: [String] = [NSLocalizedString("FILTER_DESC", comment: ""), NSLocalizedString("FILTER_ASC", comment: "")]
    let orderYears = UIPickerView()
    var orderYearsData: [String] = [NSLocalizedString("FILTER_ALL_YEARS", comment: "")]
    var successSwitch = UISwitch()
    weak var delegate: FilterProtocol?
    var filter: Filter?

    override func viewDidLoad() {
        super.viewDidLoad()
        setupDimmingView()
        setupContainerView()
        setupComponentsFilter()
        filter = FilterHelper.share.get()

        if let order = filter?.order, let index = orderPickerData.firstIndex(of: order) {
            orderPicker.selectRow(index, inComponent: 0, animated: true)
        }

        if let year = filter?.year, let index = orderYearsData.firstIndex(of: year) {
            orderYears.selectRow(index, inComponent: 0, animated: true)
        }

        if let onlySuccess = filter?.onlySuccess {
            successSwitch.setOn(onlySuccess, animated: true)
        }
    }

    @objc private func dismissSelf() {
        dismiss(animated: true, completion: nil)
    }

    private func setupDimmingView() {
        dimmingView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(dimmingView)
        NSLayoutConstraint.activate([
            dimmingView.topAnchor.constraint(equalTo: view.topAnchor),
            dimmingView.leftAnchor.constraint(equalTo: view.leftAnchor),
            dimmingView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            dimmingView.rightAnchor.constraint(equalTo: view.rightAnchor),
        ])
        dimmingView.backgroundColor = UIColor.gray.withAlphaComponent(0.6)
        dimmingView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dismissSelf)))
    }

    private func setupContainerView() {
        container.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(container)
        NSLayoutConstraint.activate([
            container.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            container.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 32),
            container.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -32),
        ])
        container.backgroundColor = .background
        container.layer.cornerRadius = 16
    }

    private func setupComponentsFilter() {
        let orderTitle = UILabel()
        orderTitle.text = NSLocalizedString("FILTER_ORDER", comment: "")
        orderTitle.accessibilityIdentifier = "orderTitle"

        orderPicker.dataSource = self
        orderPicker.delegate = self
        orderPicker.accessibilityIdentifier = "orderPicker"

        let yearTitle = UILabel()
        yearTitle.accessibilityIdentifier = "yearTitle"
        yearTitle.text = NSLocalizedString("FILTER_YEAR", comment: "")

        orderYearsData.append(contentsOf: FilterHelper.share.yearsForFilter)
        orderYears.dataSource = self
        orderYears.delegate = self
        orderYears.accessibilityIdentifier = "yearPicker"

        let successTitle = UILabel()
        successTitle.accessibilityIdentifier = "successTitle"
        successTitle.text = NSLocalizedString("FILTER_LAUNCHE_SUCCESS", comment: "")

        successSwitch.accessibilityIdentifier = "successSwitch"

        let buttonSave = UIButton()
        buttonSave.accessibilityIdentifier = "saveButton"
        buttonSave.setTitle(NSLocalizedString("FILTER_SAVE", comment: ""), for: .normal)
        buttonSave.backgroundColor = .header
        buttonSave.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(updateFilters)))

        [orderTitle, orderPicker, yearTitle, orderYears, successTitle, successSwitch, buttonSave].forEach {
            $0.translatesAutoresizingMaskIntoConstraints = false
            container.addSubview($0)
        }

        container.layer.masksToBounds = true

        NSLayoutConstraint.activate([
            // order
            orderTitle.topAnchor.constraint(equalTo: container.topAnchor, constant: 8),
            orderTitle.leftAnchor.constraint(equalTo: container.leftAnchor, constant: 8),
            orderPicker.heightAnchor.constraint(equalToConstant: 80),
            orderPicker.topAnchor.constraint(equalTo: orderTitle.bottomAnchor, constant: 8),
            orderPicker.leftAnchor.constraint(equalTo: container.leftAnchor, constant: 8),
            container.rightAnchor.constraint(equalTo: orderPicker.rightAnchor, constant: 8),
            // years
            yearTitle.topAnchor.constraint(equalTo: orderPicker.bottomAnchor, constant: 8),
            yearTitle.leftAnchor.constraint(equalTo: container.leftAnchor, constant: 8),
            orderYears.heightAnchor.constraint(equalToConstant: 80),
            orderYears.topAnchor.constraint(equalTo: yearTitle.bottomAnchor, constant: 8),
            orderYears.leftAnchor.constraint(equalTo: container.leftAnchor, constant: 8),
            container.rightAnchor.constraint(equalTo: orderYears.rightAnchor, constant: 8),
            // success
            successTitle.topAnchor.constraint(equalTo: orderYears.bottomAnchor, constant: 8),
            successTitle.leftAnchor.constraint(equalTo: container.leftAnchor, constant: 8),
            successSwitch.leftAnchor.constraint(equalTo: successTitle.rightAnchor, constant: 16),
            container.rightAnchor.constraint(equalTo: successSwitch.rightAnchor, constant: 8),
            successSwitch.centerYAnchor.constraint(equalTo: successTitle.centerYAnchor),
            // save
            buttonSave.topAnchor.constraint(equalTo: successTitle.bottomAnchor, constant: 16),
            buttonSave.leftAnchor.constraint(equalTo: container.leftAnchor),
            buttonSave.rightAnchor.constraint(equalTo: container.rightAnchor),
            buttonSave.heightAnchor.constraint(equalToConstant: 35),
            container.bottomAnchor.constraint(equalTo: buttonSave.bottomAnchor, constant: 0),
        ])
    }

    @objc func updateFilters() {
        guard var filter = filter else {
            return
        }

        filter.onlySuccess = successSwitch.isOn
        FilterHelper.share.set(filter: filter)
        delegate?.updateFilter()
        dismissSelf()
    }
}

extension FiltersPopupViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in _: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent _: Int) -> Int {
        if pickerView.isEqual(orderPicker) {
            return orderPickerData.count
        }

        return orderYearsData.count
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent _: Int) -> String? {
        if pickerView.isEqual(orderPicker) {
            return orderPickerData[row]
        }

        return orderYearsData[row]
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent _: Int) {
        if pickerView.isEqual(orderPicker) {
            filter?.order = orderPickerData[row]
        }

        filter?.year = orderYearsData[row]
    }
}
