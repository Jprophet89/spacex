//
//  PopUpSlideManager.swift
//  PopUpSlideManager
//
//  Created by Joao Fonseca on 16/08/2021.
//

import UIKit

enum PresentationDirection {
    case left
    case top
    case right
    case bottom
}

class PopUpSlideManager: NSObject {
    public var direction: PresentationDirection = .left
    public var disableCompactHeight = false
}

extension PopUpSlideManager: UIViewControllerTransitioningDelegate {
    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source _: UIViewController) -> UIPresentationController? {
        let presentationController = PopUpPresentationController(
            presentedViewController: presented,
            presenting: presenting,
            direction: direction)
        presentationController.delegate = self
        return presentationController
    }

    func animationController(forPresented _: UIViewController, presenting _: UIViewController, source _: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return PopUpPresentationAnimator(direction: direction, isPresentation: true)
    }

    func animationController(forDismissed _: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return PopUpPresentationAnimator(direction: direction, isPresentation: false)
    }
}

extension PopUpSlideManager: UIAdaptivePresentationControllerDelegate {
    func adaptivePresentationStyle(for _: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        if traitCollection.verticalSizeClass == .compact, disableCompactHeight {
            return .overFullScreen
        } else {
            return .none
        }
    }

    func presentationController(_: UIPresentationController, viewControllerForAdaptivePresentationStyle style: UIModalPresentationStyle) -> UIViewController? {
        guard case .overFullScreen = style else { return nil }
        return UIViewController()
    }
}
