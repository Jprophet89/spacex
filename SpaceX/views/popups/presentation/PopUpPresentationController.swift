//
//  PopUpPresentationController.swift
//  PopUpPresentationController
//
//  Created by Joao Fonseca on 16/08/2021.
//

import UIKit

class PopUpPresentationController: UIPresentationController {
    let direction: PresentationDirection

    override var frameOfPresentedViewInContainerView: CGRect {
        var frame: CGRect = .zero
        frame.size = size(forChildContentContainer: presentedViewController, withParentContainerSize: containerView!.bounds.size)

        switch direction {
            case .right:
                frame.origin.x = .zero
            case .bottom:
                frame.origin.y = .zero
            default:
                frame.origin = .zero
        }

        return frame
    }

    // MARK: - Initializers

    init(presentedViewController: UIViewController, presenting presentingViewController: UIViewController?, direction: PresentationDirection) {
        self.direction = direction
        super.init(presentedViewController: presentedViewController, presenting: presentingViewController)
    }

    override func containerViewWillLayoutSubviews() {
        presentedView?.frame = frameOfPresentedViewInContainerView
    }

    override func size(forChildContentContainer _: UIContentContainer, withParentContainerSize parentSize: CGSize) -> CGSize {
        return CGSize(width: parentSize.width, height: parentSize.height)
    }
}
