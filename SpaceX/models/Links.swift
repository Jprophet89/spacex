//
//  Links.swift
//  Links
//
//  Created by Joao Fonseca on 15/08/2021.
//

import Foundation

struct Links: Codable {
    var mission_patch_small: String?
    var article_link: String?
    var wikipedia: String?
    var video_link: String?
}
