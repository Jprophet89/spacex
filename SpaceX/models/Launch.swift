//
//  Launch.swift
//  Launch
//
//  Created by Joao Fonseca on 13/08/2021.
//

import Foundation

struct Launch: Codable {
    let flight_number: Int
    let mission_name: String
    let details: String?
    let launch_year: String
    let launch_date_utc: String
    let launch_success: Bool?
    let rocket: Rocket
    let upcoming: Bool
    let links: Links
}
