//
//  Filter.swift
//  Filter
//
//  Created by Joao Fonseca on 16/08/2021.
//

import Foundation

struct Filter: Codable {
    var order = NSLocalizedString("FILTER_DESC", comment: "")
    var year = NSLocalizedString("FILTER_ALL_YEARS", comment: "")
    var onlySuccess = false
}
