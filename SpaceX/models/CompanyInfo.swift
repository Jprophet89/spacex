//
//  CompanyInfo.swift
//  CompanyInfo
//
//  Created by Joao Fonseca on 13/08/2021.
//

import Foundation

struct CompanyInfo: Codable {
    let name: String
    let founder: String
    let founded: Int
    let employees: Int
    let launch_sites: Int
    let valuation: Int
}
