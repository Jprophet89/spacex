//
//  Rocket.swift
//  Rocket
//
//  Created by Joao Fonseca on 13/08/2021.
//

import Foundation

struct Rocket: Codable {
    let rocket_name: String
    let rocket_type: String
}
