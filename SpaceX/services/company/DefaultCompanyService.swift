//
//  DefaultCompanyService.swift
//  DefaultCompanyService
//
//  Created by Joao Fonseca on 13/08/2021.
//

import Foundation

class DefaultCompanyService: CompanyService {
    @Inject var repository: CompanyRepository

    func get() -> CompanyInfo? {
        return repository.getInfo()
    }

    func refresh(completion: @escaping (Result<CompanyInfo, ApiError>) -> Void) {
        repository.refresh(completion: completion)
    }
}
