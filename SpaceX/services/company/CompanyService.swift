//
//  CompanyService.swift
//  CompanyService
//
//  Created by Joao Fonseca on 13/08/2021.
//

import Foundation

// sourcery: AutoMockable
protocol CompanyService: AnyObject {
    func get() -> CompanyInfo?
    func refresh(completion: @escaping (Result<CompanyInfo, ApiError>) -> Void)
}
