//
//  LaunchService.swift
//  LaunchService
//
//  Created by Joao Fonseca on 15/08/2021.
//

import UIKit

// sourcery: AutoMockable
protocol LaunchService: AnyObject {
    func get() -> [Launch?]?
    func refresh(filter: Filter?, completion: @escaping (Result<[Launch?], ApiError>) -> Void)
    func getDate(launch: Launch?) -> Date?
    func getRocketInfo(launch: Launch?) -> String
    func getLaunchImage(launch: Launch?, completion: @escaping (Result<UIImage, ApiError>) -> Void)
}
