//
//  DefaultLaunchService.swift
//  DefaultLaunchService
//
//  Created by Joao Fonseca on 15/08/2021.
//

import SDWebImage
import UIKit

class DefaultLaunchService: LaunchService {
    @Inject var repository: LaunchRepository

    func get() -> [Launch?]? {
        return repository.get()
    }

    func refresh(filter: Filter?, completion: @escaping (Result<[Launch?], ApiError>) -> Void) {
        repository.refresh(filter: filter, completion: completion)
    }

    func getDate(launch: Launch?) -> Date? {
        guard let dateString = launch?.launch_date_utc, let date = DateHelper.getDate(dateString) else {
            return nil
        }

        return date
    }

    func getRocketInfo(launch: Launch?) -> String {
        let notAvailable = NSLocalizedString("NOT_AVAILABLE", comment: "")
        return "\(launch?.rocket.rocket_name ?? notAvailable) / \(launch?.rocket.rocket_type ?? notAvailable)"
    }

    func getLaunchImage(launch: Launch?, completion: @escaping (Result<UIImage, ApiError>) -> Void) {
        // Set Rocket default if null
        guard let link = launch?.links.mission_patch_small, let url = URL(string: link) else {
            if let rocket = UIImage(named: "rocket") {
                completion(.success(rocket))
            } else {
                completion(.failure(.unknown))
            }
            return
        }

        // Check in cache
        if let image = SDImageCache.shared.imageFromCache(forKey: link) {
            completion(.success(image))
        }

        // Download and cache the image
        let launchImage = UIImageView()
        launchImage.sd_setImage(with: url, completed: { image, _, _, _ in
            guard let image = image else {
                if let rocket = UIImage(named: "rocket") {
                    completion(.success(rocket))
                } else {
                    completion(.failure(.unknown))
                }
                return
            }

            completion(.success(image))
            SDImageCache.shared.store(image, forKey: link, completion: nil)
        })
    }
}
