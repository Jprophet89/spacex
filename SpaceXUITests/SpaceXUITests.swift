//
//  SpaceXUITests.swift
//  SpaceXUITests
//
//  Created by Joao Fonseca on 13/08/2021.
//

import XCTest

class SpaceXUITests: XCTestCase {
    override func setUpWithError() throws {
        continueAfterFailure = false
    }

    func testShouldHaveElements() throws {
        let app = XCUIApplication()
        app.launch()

        XCTAssert(app.buttons["filterButton"].exists)
        XCTAssert(app.tables.containing(.other, identifier: "companyHeader").element.exists)
        XCTAssert(app.tables.containing(.other, identifier: "launchesHeader").element.exists)
        XCTAssert(app.tables.cells["companyInformationCell"].exists)
    }

    func testShouldOpenFilters() throws {
        let app = XCUIApplication()
        app.launch()
        let filtersButton = app.buttons["filterButton"]
        XCTAssert(filtersButton.exists)
        filtersButton.tap()

        XCTAssert(app.staticTexts["orderTitle"].exists)
        XCTAssert(app.pickers["orderPicker"].exists)
        XCTAssert(app.staticTexts["yearTitle"].exists)
        XCTAssert(app.pickers["yearPicker"].exists)
        XCTAssert(app.staticTexts["successTitle"].exists)
        XCTAssert(app.switches["successSwitch"].exists)
        let saveButton = app.buttons["saveButton"]
        XCTAssert(saveButton.exists)

        saveButton.tap()

        XCTAssertFalse(saveButton.exists)
    }

    func testShouldLaunchDetail() throws {
        let app = XCUIApplication()
        app.launch()
        let mainTable = app.tables.matching(identifier: "mainTable").element
        XCTAssert(mainTable.exists)
        let cell = app.cells["launchCell3"]
        XCTAssert(cell.exists)
        cell.tap()
        let missionImage = app.images["missionImage"]
        XCTAssert(missionImage.exists)
        let missionName = app.staticTexts["missionName"]
        XCTAssert(missionName.exists)
        let informationDetails = app.staticTexts["informationDetails"]
        XCTAssert(informationDetails.exists)
        app.swipeDown(velocity: .fast)
        XCTAssertFalse(missionImage.exists)
        XCTAssertFalse(missionName.exists)
        XCTAssertFalse(informationDetails.exists)
    }
}
